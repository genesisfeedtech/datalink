﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using BuchiConsole;
using NLog;

namespace BuchiService
{
    public class Service : ServiceBase
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly Coordinator coordinator;

        public Service()
        {
            InitializeComponent();
            coordinator = new Coordinator();
        }

        protected override void OnStart(string[] args)
        {
            logger.Info("Service.OnStart");
            try
            {
                coordinator.Start();

                logger.Info("Service.OnStart success");
            }
            catch (Exception e)
            {
                logger.Error(e, "Service.OnStart exception thrown");
            }
        }

        protected override void OnStop()
        {
        }

        private void InitializeComponent()
        {
            logger.Debug("Service.InitializeComponent");
            // 
            // Service
            // 
            this.ServiceName = "GFT Buchi Datalink";
        }
    }
}