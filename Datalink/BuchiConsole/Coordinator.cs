﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Common.IPC;
using NLog;

namespace BuchiConsole
{
    public class Coordinator
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private Credentials Credentials;
        private Http http;
        private BuchiConfiguration Config;

        private Boolean? ConnectionSuccessful = null;
        private DateTimeOffset nextSync = DateTimeOffset.MinValue;
        private DateTimeOffset? LastHeartbeat = null;

        private DateTimeOffset NextHeartbeat = DateTimeOffset.MinValue;
        private DateTimeOffset? LastSync = null;
        private List<string> Messages = new List<string>();

        private Thread Heartbeat;
        private Thread Sync;
        private Thread Tray;              

        public Coordinator()
        {
            SetConnection(new Credentials(ApplicationConfig.Get("host"), ApplicationConfig.Get("token")));
        }

        public void SetConnection(Credentials credentials)
        {
            this.Credentials = credentials;
            http = new Http(credentials.ServerUrl, credentials.ApiKey);            
        }

        private void SaveConfig()
        {            
            ApplicationConfig.Set("host", Credentials?.ServerUrl);
            ApplicationConfig.Set("token", Credentials?.ApiKey);
        }

        public void Start()
        {
            logger.Info("Start");

            StartTrayUpdate();
            StartHeartbeat();
            StartSync();
        }

        private async Task TestPlatformConnectionAsync()
        {
            if (http != null)
            {
                var response = await http.GetAsync("/api/v1/integration/current");
                logger.Info("Test Connection: {0} - {1}", response.Status, response.Reason);
                this.ConnectionSuccessful = response.Is(200);
                if (this.ConnectionSuccessful.GetValueOrDefault(false))
                {
                    this.Messages.Add("Test Connection: SUCCESS");
                }
                else
                {
                    this.Messages.Add($"Test Connection: FAILED - Response was ${response.Status} (${response.Reason})");
                }
            }
            else
            {
                this.Messages.Add("Test Connection: FAILED - Connection info not set!");
                this.ConnectionSuccessful = false;
            }
        }

        private async Task LoadConfigurationAsync()
        {
            bool autoLog = !ConnectionSuccessful.HasValue;
            bool prior = ConnectionSuccessful.GetValueOrDefault(false);
            String message = String.Empty;
            if (this.Credentials.ServerUrl == null || this.Credentials.ApiKey == null)
            {
                this.ConnectionSuccessful = false;
                this.Config = null;
                logger.Debug("Invalid credentials {0}, {1}", this.Credentials.ServerUrl, this.Credentials.ApiKey);
            }
            else
            {
                HttpResponse response = await http.GetAsync("/api/v1/integration/current/detail");
                var body = await response.GetBodyAsync();
                this.ConnectionSuccessful = response.Is(200);
                if (this.ConnectionSuccessful.GetValueOrDefault(false))
                {
                    logger.Info("getConfig: {0}", body);
                    Config = new BuchiConfiguration(body);
                }
                else
                {
                    message = $"Configuration not loaded - {response.Status}: {response.Reason}{body?.GetString("message")}";
                    logger.Warn(message);
                    Config = null;
                }
            }
            
            if (autoLog || ConnectionSuccessful.Value != prior)
            {
                Messages.Add("PlatformConnection " + (ConnectionSuccessful.Value ? "Ok" : "Failed"));
                if (!ConnectionSuccessful.Value)
                {
                    Messages.Add(message);
                }
            }
        }

        private void StartHeartbeat()
        {
            Heartbeat = new Thread(new ThreadStart(DoHeartbeat));
            Heartbeat.Start();                 
        }

        async void DoHeartbeat()
        {
            Thread.CurrentThread.IsBackground = false;            
            Thread.CurrentThread.Name = "Heartbeat";
            while (true)
            {
                try
                {
                    if (Config != null && DateTimeOffset.Now > this.NextHeartbeat)
                    {
                        logger.Debug("Heartbeat: doPost");
                        HttpResponse response = await http.PostAsync("/api/v1/integration/current/heartbeat");
                        if (response.Is(200))
                        {
                            LastHeartbeat = DateTimeOffset.Now;
                        }
                        else
                        {
                            var body = await response.GetBodyAsync();
                            var message = "Heartbeat failed - " + response.Status + ": " + response.Reason + body?.GetString("message");
                            Messages.Add(message);
                            logger.Warn(message);
                        }

                        NextHeartbeat = DateTimeOffset.Now.Add(Config.GetHeartbeatInterval());
                        logger.Debug("Next heartbeat scheduled at {0}", NextHeartbeat);
                    }
                }
                catch (Exception x)
                {
                    logger.Error(x, "Exception thrown in Heartbeat loop");
                }
                finally
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                }
            }
        }


        private void StartSync()
        {
            Sync = new Thread(new ThreadStart(DoSync));
            Sync.Start();
        }

        async void DoSync()
        {
            Thread.CurrentThread.Name = "Sync";
            while (true)
            {
                try
                {
                    if (!this.ConnectionSuccessful.HasValue)
                    {
                        await TestPlatformConnectionAsync();
                    }
                    
                    if (DateTimeOffset.Now > this.nextSync)
                    {
                        logger.Debug("Sync: updateConfig");
                        await LoadConfigurationAsync();
                        if (this.ConnectionSuccessful.GetValueOrDefault(false))
                        {
                                var stopwatch = Stopwatch.StartNew();

                                HttpResponse startResponse = await SendSyncStarted();
                                bool success = true;
                                try
                                {
                                    var dataReader = new DataReader(Config, http);
                                    await dataReader.ExecuteAsync();
                                    Messages.AddRange(dataReader.GetMessages());
                                }
                                catch (Exception e)
                                {
                                    logger.Warn(e, "Exception thrown while executing readers");
                                    success = false;
                                }
                                finally
                                {
                                    SendSyncComplete(success);
                                    LastSync = DateTimeOffset.Now;

                                    stopwatch.Stop();
                                    string flag = success ? "completed" : "failed";
                                    Messages.Add($"Sync {flag}, {stopwatch.Elapsed.TotalSeconds:0.0}s elapsed");
                                }

                        }

                        TimeSpan interval = Config == null ? TimeSpan.FromMinutes(10) : Config.GetSyncInterval();
                        this.nextSync = DateTimeOffset.Now.Add(interval);
                        logger.Debug("Sync: sleep {0} seconds", interval.TotalSeconds);
                    }
                }
                catch (Exception x)
                {
                    logger.Error(x, "Exception thown in sync loop");
                }
                finally
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                }
            }
        }

        private async Task<HttpResponse> SendSyncStarted()
        {
            return await http.PostAsync("/api/v1/integration/current/syncStarted");
        }
        
        private async Task<HttpResponse> SendSyncComplete(bool success)
        {
            return await http.PostAsync($"/api/v1/integration/current/syncComplete?success={success}");
        }

        private void StartTrayUpdate()
        {
            Tray = new Thread(new ThreadStart(DoUpdateTray));
            Tray.Start();
        }

        private async void DoUpdateTray()
        {
            Thread.CurrentThread.Name = "UpdateTray";
            while (true)
            {
                try
                {                    
                    ChannelFactory<ITrayService> pipeFactory = new ChannelFactory<ITrayService>(new NetNamedPipeBinding(), new EndpointAddress("net.pipe://localhost/GenesisFeedTech_DatalinkTray"));

                    ITrayService pipeProxy = pipeFactory.CreateChannel();

                    var status = new Status()
                    {
                        Credentials = this.Credentials,
                        PlatformConnectionSuccessful = ConnectionSuccessful.GetValueOrDefault(false),
                        LastHeartbeat = LastHeartbeat,
                        LastSync = LastSync,
                        Messages = new List<string>(Messages)
                    };                    
                    Messages.RemoveRange(0, status.Messages.Count);

                    var command = pipeProxy.UpdateStatus(status);
                    if (command.UpdatedCredentials != null)
                    {
                        logger.Info("Credentials updated");
                        this.SetConnection(command.UpdatedCredentials);
                        this.SaveConfig();
                        this.nextSync = DateTimeOffset.MinValue;
                    }
                    if (command.ForceSync)
                    {
                        logger.Info("Immediate sync requested");
                        this.nextSync = DateTimeOffset.MinValue;
                        this.ConnectionSuccessful = null;
                    }

                    Thread.Sleep(TimeSpan.FromMilliseconds(100));
                }
                catch (Exception e)
                {
                    logger.Debug(e, "IPC Connection Failed");
                    //Sleep longer if the connection failed
                    Thread.Sleep(TimeSpan.FromMilliseconds(1000));
                }
            }
        }

    }
}
