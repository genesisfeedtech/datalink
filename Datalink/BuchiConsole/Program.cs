﻿using Common.Logging;

namespace BuchiConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            new Logging().Init();

            Coordinator coordinator = new Coordinator();
            coordinator.Start();
        }
    }
}
