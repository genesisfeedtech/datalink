﻿using System;
using Common;

namespace BuchiConsole
{
    public class BuchiConfiguration : Configuration
    {
        public BuchiConfiguration(JsonModel integrationDetailResponse) : base(integrationDetailResponse)
        {
        }

        public String GetWatchDirectory()
        {
            return GetValue("watchDirectory", null);
        }
        
    }
}
