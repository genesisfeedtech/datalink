﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Http;
using System.Net.Mime;
using System.Runtime.Remoting.Messaging;
using System.Security;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Common;
using NLog;

namespace BuchiConsole
{
    class DataReader
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private String Api = "/api/integration/buchi/current/submitReading";
        private readonly BuchiConfiguration config;
        private readonly Http http;


        private int failedPostCount = 0;
        private int count = 0;
        private List<string> Messages = new List<string>();

        public DataReader(BuchiConfiguration config, Http http)
        {
            this.config = config;
            this.http = http;
        }

        public async Task ExecuteAsync()
        {
            if (config == null)
            {
                Messages.Add("Configuration not loaded");
                logger.Error("Abort: Configuration not loaded");
            } else if (string.IsNullOrEmpty(config.GetWatchDirectory()))
            {
                Messages.Add("Watch Directory is not set");
                logger.Error("Abort: Watch Directory is not set");
            }
            else
            {
                await ScanDirectory();
            }
        }

        private async Task ScanDirectory()
        {
            try
            {
                DirectoryInfo directory = new DirectoryInfo(config.GetWatchDirectory());

                logger.Debug("Scan directory {0}", directory.FullName);
                if (!directory.Exists)
                {
                    Messages.Add($"Watch Directory does not exist: {directory.FullName}");
                    logger.Error("Directory not found: {0}", directory.FullName);
                }
                else if ((directory.Attributes & FileAttributes.Directory) == 0)
                {
                    Messages.Add($"Invalid Watch Directory {directory.FullName}");
                    logger.Error("WatchDirectory is not directory: {0}", directory.FullName);
                }
                else
                {
                    FileInfo[] files = directory.GetFiles("*.xml", SearchOption.AllDirectories);
                    if (files == null)
                    {
                        logger.Error("Cannot read directory {0} (files==null)", directory.FullName);
                    }
                    else
                    {
                        count = files.Length;
                        Messages.Add($"{count} samples found");
                        foreach (FileInfo file in files)
                        {
                            await PostFile(file);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Messages.Add($"DataReader failed: ${e.Message}");
                logger.Error(e, "DataReader failed");
            }
            finally
            {
                if (failedPostCount > 0)
                {
                    Messages.Add($"When submitting data, encountered {failedPostCount} failures");
                }
            }
        }

        private async Task PostFile(FileInfo file)
        {
            try
            {
                logger.Info("Submit file {0}", file.FullName);
                if (file.Length == 0)
                {
                    logger.Warn("file {0} is empty", file.FullName);
                    Delete(file);
                }
                else
                {
                    HttpContent content = new StringContent(File.ReadAllText(file.FullName), Encoding.UTF8, "application/xml");
                    var response = await http.PostAsync(Api, content);
                    if (response.Is(200))
                    {
                        Delete(file);
                    }
                    else
                    {
                        this.Messages.Add($"Sample submission failed for {file.FullName} : {response.Status}");
                        logger.Warn($"Sample submission failed for {file.FullName}: {response.Status} - {response.GetBodyAsString()}");
                        failedPostCount += 1;
                    }
                }
            }
            catch (IOException e)
            {
                logger.Error(e, "Failed to read file {0}", file.FullName);
            }
        }

        private void Delete(FileInfo file)
        {
            try
            {
                file.Delete();
                logger.Debug("Successfully deleted {0}", file.FullName);
            }
            catch (Exception e)
            {
                logger.Error(e, "Failed to delete {0}", file.FullName);
                try
                {
                    File.Move(file.FullName, file.FullName+".DONE");
                    logger.Info("Successfully renamed {}", file.FullName);
                    
                }
                catch (Exception e1)
                {
                    logger.Warn("Failed to rename file {0}", file.FullName);
                }
            }
        }

        public List<String> GetMessages()
        {
            return Messages;
        }
        
    }
}