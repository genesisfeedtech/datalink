﻿using System;
using System.Collections.Generic;
using Common;
using NLog;

namespace BrillSyncConsole
{
    public class BrillSyncConfiguration : Configuration
    {
        public readonly String NONE = "NoneAllowedDoNotMatchAnythingMakeThisStringLongerThanFiftyCharacters";
        public readonly String CONFIGOPTION_ALLOWED_PLANT_CODES = "allowedPlantCodes";
        public readonly String CONFIGOPTION_ALLOWED_USERNAMES = "allowedUserCodes";
        public readonly String CONFIGOPTION_SQLCONNECTION = "sqlConnectionString";

        IDictionary<String, JsonModel> Config;

        public BrillSyncConfiguration(JsonModel integrationDetailResponse) : base(integrationDetailResponse)
        {
        }

        private ISet<String> GetRestriction(String configOption)
        {
            ISet<String> result = new HashSet<String>();
            String codes = GetValue(configOption);
            if (codes != null)
            {
                foreach (String s in codes.Split(','))
                {
                    String code = s.Trim();
                    if (code.Length > 0)
                    {
                        result.Add(code);
                    }
                }
            }

            if (result.Count == 0)
            {
                result.Add(NONE);
            }
            return result;
        }

        public bool IsRestrictByPlant()
        {
            String s = GetValue(CONFIGOPTION_ALLOWED_PLANT_CODES);
            return !"*".Equals(s);
        }

        public ISet<String> GetAllowedPlants()
        {
            return GetRestriction(CONFIGOPTION_ALLOWED_PLANT_CODES);
        }

        public bool IsRestrictByUser()
        {
            String s = GetValue(CONFIGOPTION_ALLOWED_USERNAMES);
            return !"*".Equals(s);
        }

        public ISet<String> GetAllowedUsers()
        {
            return GetRestriction(CONFIGOPTION_ALLOWED_USERNAMES);
        }

        public String GetSqlConnectionString()
        {
            return GetValue(CONFIGOPTION_SQLCONNECTION);
        }
    }
}
