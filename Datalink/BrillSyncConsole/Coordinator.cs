﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using BrillSyncConsole.DataReader;
using BrillSyncConsole.DataReader.PlantCode;
using BrillSyncConsole.DataReader.StoredFormula;
using BrillSyncConsole.DataReader.UserCode;
using Common;
using Common.IPC;
using NLog;

namespace BrillSyncConsole
{
    public class Coordinator
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private Credentials Credentials;
        private Http http;
        private BrillSyncConfiguration Config;

        private Boolean? ConnectionSuccessful = null;
        private Boolean? DbConnectionSuccessful = null;
        private DateTimeOffset nextSync = DateTimeOffset.MinValue;
        private DateTimeOffset? LastHeartbeat = null;

        private DateTimeOffset NextHeartbeat = DateTimeOffset.MinValue;
        private DateTimeOffset? LastSync = null;
        private List<string> Messages = new List<string>();

        private Thread Heartbeat;
        private Thread Sync;
        private Thread Tray;              

        public Coordinator()
        {
            SetConnection(new Credentials(ApplicationConfig.Get("host"), ApplicationConfig.Get("token")));
        }

        public void SetConnection(Credentials credentials)
        {
            this.Credentials = credentials;
            http = new Http(credentials.ServerUrl, credentials.ApiKey);            
        }

        private void SaveConfig()
        {            
            ApplicationConfig.Set("host", Credentials?.ServerUrl);
            ApplicationConfig.Set("token", Credentials?.ApiKey);
        }

        public void Start()
        {
            logger.Info("Start");

            StartTrayUpdate();
            StartHeartbeat();
            StartSync();
        }

        private async Task TestPlatformConnectionAsync()
        {
            if (http != null)
            {
                var response = await http.GetAsync("/api/v1/integration/current");
                logger.Info("Test Connection: {0} - {1}", response.Status, response.Reason);
                this.ConnectionSuccessful = response.Is(200);
            }
            else
            {
                this.ConnectionSuccessful = false;
            }
        }

        private async Task LoadConfigurationAsync()
        {
            bool autoLog = !ConnectionSuccessful.HasValue;
            bool prior = ConnectionSuccessful.GetValueOrDefault(false);
            String message = String.Empty;
            if (this.Credentials.ServerUrl == null || this.Credentials.ApiKey == null)
            {
                this.ConnectionSuccessful = false;
                this.Config = null;
                logger.Debug("Invalid credentials {0}, {1}", this.Credentials.ServerUrl, this.Credentials.ApiKey);
            }
            else
            {
                HttpResponse response = await http.GetAsync("/api/v1/integration/current/detail");
                var body = await response.GetBodyAsync();
                this.ConnectionSuccessful = response.Is(200);
                if (this.ConnectionSuccessful.GetValueOrDefault(false))
                {
                    logger.Info("getConfig: {0}", body);
                    Config = new BrillSyncConfiguration(body);
                }
                else
                {
                    message = $"Configuration not loaded - {response.Status}: {response.Reason}{body?.GetString("message")}";
                    logger.Warn(message);
                    Config = null;
                }
            }
            
            if (autoLog || ConnectionSuccessful.Value != prior)
            {
                Messages.Add("PlatformConnection " + (ConnectionSuccessful.Value ? "Ok" : "Failed"));
                if (!ConnectionSuccessful.Value)
                {
                    Messages.Add(message);
                }
            }
        }

        private void StartHeartbeat()
        {
            Heartbeat = new Thread(new ThreadStart(DoHeartbeat));
            Heartbeat.Start();                 
        }

        async void DoHeartbeat()
        {
            Thread.CurrentThread.IsBackground = false;            
            Thread.CurrentThread.Name = "Heartbeat";
            while (true)
            {
                try
                {
                    if (Config != null && DateTimeOffset.Now > this.NextHeartbeat)
                    {
                        logger.Debug("Heartbeat: doPost");
                        HttpResponse response = await http.PostAsync("/api/v1/integration/current/heartbeat");
                        if (response.Is(200))
                        {
                            LastHeartbeat = DateTimeOffset.Now;
                        }
                        else
                        {
                            var body = await response.GetBodyAsync();
                            var message = "Heartbeat failed - " + response.Status + ": " + response.Reason + body?.GetString("message");
                            Messages.Add(message);
                            logger.Warn(message);
                        }

                        NextHeartbeat = DateTimeOffset.Now.Add(Config.GetHeartbeatInterval());
                        logger.Debug("Next heartbeat scheduled at {0}", NextHeartbeat);
                    }
                }
                catch (Exception x)
                {
                    logger.Error(x, "Exception thrown in Heartbeat loop");
                }
                finally
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                }
            }
        }


        private void StartSync()
        {
            Sync = new Thread(new ThreadStart(DoSync));
            Sync.Start();
        }

        async void DoSync()
        {
            Thread.CurrentThread.Name = "Sync";
            while (true)
            {
                try
                {
                    if (DateTimeOffset.Now > this.nextSync)
                    {
                        logger.Debug("Sync: updateConfig");
                        await LoadConfigurationAsync();
                        if (this.ConnectionSuccessful.GetValueOrDefault(false))
                        {
                            CheckDBConnection();
                            if (this.DbConnectionSuccessful.GetValueOrDefault(false))
                            {
                                var stopwatch = Stopwatch.StartNew();

                                HttpResponse startResponse = await http.PostAsync("/api/v1/integration/current/syncStarted");
                                IDictionary<string, string> checkpoints = new Dictionary<string, string>();
                                bool success = true;
                                try
                                {
                                    await new PlantCodeReader(Config, http).ExecuteAsync();
                                    await new UserCodeReader(Config, http).ExecuteAsync();

                                    await new IngredientDefinitionReader(Config, http).ExecuteAsync(checkpoints);
                                    await new IngredientNutrientReader(Config, http).ExecuteAsync(checkpoints);

                                    await new IngredientPricesReader(Config, http).ExecuteAsync(checkpoints);

                                    await new FormulaSpecReader(Config, http).ExecuteAsync(checkpoints);
                                    await new FormulaSpecIngredientReader(Config, http).ExecuteAsync(checkpoints);
                                    await new FormulaSpecNutrientReader(Config, http).ExecuteAsync(checkpoints);

                                    await new StoredFormulaReader(Config, http).ExecuteAsync(checkpoints);

                                    await new ProductionFormulaReader(Config, http).ExecuteAsync(checkpoints);

                                    await new SolutionReader(Config, http).ExecuteAsync(checkpoints);
                                    await new SolutionFormulaReader(Config, http).ExecuteAsync(checkpoints);
                                    await new SolutionFormulaComboGroupReader(Config, http).ExecuteAsync(checkpoints);
                                    await new SolutionFormulaIngredientReader(Config, http).ExecuteAsync(checkpoints);
                                    await new SolutionFormulaNutrientReader(Config, http).ExecuteAsync(checkpoints);
                                    await new SolutionGlobalReader(Config, http).ExecuteAsync(checkpoints);
                                    await new SolutionGlobalGroupReader(Config, http).ExecuteAsync(checkpoints);
                                    await new SolutionPlantReader(Config, http).ExecuteAsync(checkpoints);
                                    await new SolutionPlantGroupReader(Config, http).ExecuteAsync(checkpoints);

                                    await UpdateConfig(checkpoints);
                                }
                                catch (Exception e)
                                {
                                    logger.Warn(e, "Exception thrown while executing readers");
                                    success = false;
                                }
                                finally
                                {
                                    SendSyncComplete(success);
                                    LastSync = DateTimeOffset.Now;

                                    stopwatch.Stop();
                                    string flag = success ? "completed" : "failed";
                                    Messages.Add($"Sync {flag}, {stopwatch.Elapsed.TotalSeconds:0.0}s elapsed");
                                }
                            }
                        }

                        TimeSpan interval = Config == null ? TimeSpan.FromMinutes(10) : Config.GetSyncInterval();
                        this.nextSync = DateTimeOffset.Now.Add(interval);
                        logger.Debug("Sync: sleep {0} seconds", interval.TotalSeconds);
                    }
                }
                catch (Exception x)
                {
                    logger.Error(x, "Exception thown in sync loop");
                }
                finally
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                }
            }
        }

        private async void SendSyncComplete(bool success)
        {
            try
            {
                HttpResponse completeResponse = await http.PostAsync($"/api/v1/integration/current/syncComplete?success={success}");
            }
            catch (Exception e)
            {
                logger.Warn(e, "Failed to send sync completed response");
            }
        }

        private async Task UpdateConfig(IDictionary<string, string> options)
        {
            IDictionary<string, object> request = new Dictionary<string, object>();
            var configOptions = new List<IDictionary<string, object>>();

            foreach (var entry in options)
            {
                IDictionary<string, object> optionRequest = new Dictionary<string, object>()
                {
                    { "name", entry.Key },
                    { "value", entry.Value }
                };

                configOptions.Add(optionRequest);

            }
            request["configOptions"] = configOptions;

            var updateResponse = await http.PostAsync("/api/v1/integration/current", request);
            if (updateResponse.Is(200))
            {
                logger.Debug("ConfigOptions updated ok");
            } 
            else
            {
                var body = await updateResponse.GetBodyAsync();
                var message = $"Config update failed - {updateResponse.Status}: {updateResponse.Reason}{body?.GetString("message")}";
                logger.Warn(message);
            }
        }

        private void CheckDBConnection()
        {
            bool autoLog = !DbConnectionSuccessful.HasValue;
            bool prior = DbConnectionSuccessful.GetValueOrDefault(false);
            string connectionFailedMessage = String.Empty;
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(Config.GetSqlConnectionString()))
                {
                    sqlConnection.Open();
                    sqlConnection.Close();
                }
                DbConnectionSuccessful = true;
            }
            catch (Exception e)
            {
                DbConnectionSuccessful = false;
                connectionFailedMessage = e.Message;
            }
            
            if (autoLog || DbConnectionSuccessful.Value != prior)
            {
                Messages.Add("DbConnection " + (DbConnectionSuccessful.Value ? "Ok" : "Failed"));
                if (!String.IsNullOrWhiteSpace(connectionFailedMessage))
                {
                    Messages.Add(connectionFailedMessage);
                }
            }
        }

        private void StartTrayUpdate()
        {
            Tray = new Thread(new ThreadStart(DoUpdateTray));
            Tray.Start();
        }

        private async void DoUpdateTray()
        {
            Thread.CurrentThread.Name = "UpdateTray";
            while (true)
            {
                try
                {                    
                    ChannelFactory<ITrayService> pipeFactory = new ChannelFactory<ITrayService>(
                     new NetNamedPipeBinding(), new EndpointAddress("net.pipe://localhost/GenesisFeedTech_DatalinkTray"));

                    ITrayService pipeProxy = pipeFactory.CreateChannel();

                    var status = new Status()
                    {
                        Credentials = this.Credentials,
                        PlatformConnectionSuccessful = ConnectionSuccessful.GetValueOrDefault(false),
                        LastHeartbeat = LastHeartbeat,
                        LastSync = LastSync,
                        Messages = new List<string>(Messages)
                    };                    
                    Messages.Clear();  //todo this should really be atomic with the building of the status object

                    var command = pipeProxy.UpdateStatus(status);
                    if (command.UpdatedCredentials != null)
                    {
                        logger.Info("Credentials updated");
                        this.SetConnection(command.UpdatedCredentials);
                        this.SaveConfig();
                        this.nextSync = DateTimeOffset.MinValue;
                    }
                    if (command.ForceSync)
                    {
                        logger.Info("Immediate sync requested");
                        this.nextSync = DateTimeOffset.MinValue;
                        this.ConnectionSuccessful = null;
                        this.DbConnectionSuccessful = null;
                    }

                    Thread.Sleep(TimeSpan.FromMilliseconds(100));
                }
                catch (Exception e)
                {
                    logger.Debug(e, "IPC Connection Failed");
                    //Sleep longer if the connection failed
                    Thread.Sleep(TimeSpan.FromMilliseconds(1000));
                }
            }
        }

    }
}
