﻿using Common.Logging;
using NLog;
using System.Diagnostics;
using BrillSyncConsole;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            new Logging().Init();

            Coordinator coordinator = new Coordinator();
            coordinator.Start();
        }
    }
}
