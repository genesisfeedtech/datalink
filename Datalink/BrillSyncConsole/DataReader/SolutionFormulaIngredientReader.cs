﻿using System;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class SolutionFormulaIngredientReader : EFReader<FMS_SolutionFormulaIngredient>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        new private const String Api = "/api/integration/brill/solutionFormulaIngredient/add";
        private DateTime? LastCreationDate = null;

        public SolutionFormulaIngredientReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
            LastCreationDate = config.GetDateTime(SolutionReader.CheckpointConfig, LastCreationDate);
            if (LastCreationDate.HasValue)
            {
                logger.Info("Resume at {0}", LastCreationDate);
            }
        }

        protected override IQueryable<FMS_SolutionFormulaIngredient> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_SolutionFormulaIngredient> query = databridge.FMS_SolutionFormulaIngredient;
            if (Config.IsRestrictByUser())
            {
                var users = Config.GetAllowedUsers();
                query = query.Where(s => users.Contains(s.SolutionFormula.Solution.UserName));
            }
            if (LastCreationDate.HasValue)
            {
                query = query.Where(i => i.SolutionFormula.Solution.CreationDate >= LastCreationDate.Value);
            }
            query = query.OrderBy(i => i.SolutionFormula.Solution.CreationDate);

            return query;
        }

        protected override object GetCheckpointValue()
        {
            return LastCreationDate;
        }
    }
}
