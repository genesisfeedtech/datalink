﻿using System;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class SolutionPlantGroupReader : EFReader<FMS_SolutionPlantGroup>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        new private const String Api = "/api/integration/brill/solutionPlantGroup/add";
        private DateTime? LastCreationDate = null;

        public SolutionPlantGroupReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
            LastCreationDate = config.GetDateTime(SolutionReader.CheckpointConfig, LastCreationDate);
            if (LastCreationDate.HasValue)
            {
                logger.Info("Resume at {0}", LastCreationDate);
            }
        }

        protected override IQueryable<FMS_SolutionPlantGroup> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_SolutionPlantGroup> query = databridge.FMS_SolutionPlantGroup;
            if (Config.IsRestrictByUser())
            {
                var users = Config.GetAllowedUsers();
                query = query.Where(s => users.Contains(s.Solution.UserName));
            }
            if (LastCreationDate.HasValue)
            {
                query = query.Where(f => f.Solution.CreationDate >= LastCreationDate.Value);
            }
            query = query.OrderBy(f => f.Solution.CreationDate);

            return query;
        }

        protected override object GetCheckpointValue()
        {
            return LastCreationDate;
        }
    }
}
