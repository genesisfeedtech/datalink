﻿using System;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class FormulaSpecReader : EFReader<FMS_FormulaSpec>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();
        new private const String Api = "/api/integration/brill/formulaSpec/add";

        public FormulaSpecReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
        }

        protected override IQueryable<FMS_FormulaSpec> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_FormulaSpec> query = databridge.FMS_FormulaSpec;
            if (Config.IsRestrictByPlant())
            {
                var plants = Config.GetAllowedPlants();
                query = query.Where(f => plants.Contains(f.PlantCode));
            }

            return query;
        }
    }
}
