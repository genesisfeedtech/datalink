﻿using System;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class FormulaSpecNutrientReader : EFReader<FMS_FormulaSpecNutrient>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();
        new private const String Api = "/api/integration/brill/formulaSpecNutrient/add";

        public FormulaSpecNutrientReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
        }

        protected override IQueryable<FMS_FormulaSpecNutrient> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_FormulaSpecNutrient> query = databridge.FMS_FormulaSpecNutrient;
            if (Config.IsRestrictByPlant())
            {
                var plants = Config.GetAllowedPlants();
                query = query.Where(f => plants.Contains(f.FormulaSpec.PlantCode));
            }

            return query;
        }
    }
}
