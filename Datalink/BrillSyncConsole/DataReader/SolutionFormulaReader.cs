﻿using System;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class SolutionFormulaReader : EFReader<FMS_SolutionFormula>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        new private const String Api = "/api/integration/brill/solutionFormula/add";
        private DateTime? LastCreationDate = null;

        public SolutionFormulaReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
            LastCreationDate = config.GetDateTime(SolutionReader.CheckpointConfig, LastCreationDate);
            if (LastCreationDate.HasValue)
            {
                logger.Info("Resume at {0}", LastCreationDate);
            }
        }
        
        protected override IQueryable<FMS_SolutionFormula> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_SolutionFormula> query = databridge.FMS_SolutionFormula;
            if (Config.IsRestrictByUser())
            {
                var users = Config.GetAllowedUsers();
                query = query.Where(s => users.Contains(s.Solution.UserName));
            }
            if (LastCreationDate.HasValue)
            {
                query = query.Where(f => f.Solution.CreationDate >= LastCreationDate.Value);
            }
            query = query.OrderBy(f => f.Solution.CreationDate);

            return query;
        }

        protected override object GetCheckpointValue()
        {
            return LastCreationDate;
        }
    }
}
