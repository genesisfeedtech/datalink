﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    public abstract class EFReader<T>
    {
        private readonly Logger Logger;
        protected readonly String Api;
        protected readonly BrillSyncConfiguration Config;
        protected readonly Http Http;

        private string PriorCountProperty { get
            {
                return $"PriorCount_{GetType().Name}";
            }
        }
        public int BatchSize { get; set; } = 50;
        private int batchCount = 0;
        private int priorCount = 0;
        private int totalCount = 0;

        private object priorCheckpointValue = null;

        public EFReader(BrillSyncConfiguration config, Http http, String api, Logger logger)
        {
            this.Config = config;
            this.Http = http;
            this.Api = api;
            this.Logger = logger;

            priorCount = config.GetInt(PriorCountProperty, priorCount).GetValueOrDefault(0);
            if (priorCount > 0)
            {
                logger.Info("{0} prior records", priorCount);
            }
        }

        public async Task ExecuteAsync(IDictionary<string, string> checkpoints)
        {
            if (Config == null)
            {
                Logger.Error("Abort: Configuration not loaded");
                return;
            }
            var stopwatch = Stopwatch.StartNew();
            try
            {
                using (DataBridge databridge = new DataBridge(Config.GetSqlConnectionString()))
                {
                    var currentCount = GetEntities(databridge).Count();
                    if (priorCount == currentCount)
                    {
                        Logger.Info("{0} records: unchanged", currentCount);
                        return;
                    }
                    else
                    {
                        this.priorCheckpointValue = GetCheckpointValue();
                        List<Object> content = new List<Object>();
                        foreach (T model in GetEntities(databridge))
                        {
                            Checkpoint(model);
                            content.Add(ToRequest(model));
                            if (content.Count >= BatchSize)
                            {
                                bool success = await PostBatch(content);
                                content.Clear();
                                if (!success)
                                {
                                    return;
                                }
                            }
                        }

                        await PostBatch(content);
                        Logger.Debug("prior: {0}, current: {1}, total: {2}", priorCount, currentCount, totalCount);
                        priorCount = totalCount;
                    }
                }

                UpdateCheckpoints(checkpoints);
            }
            finally
            {
                stopwatch.Stop();
                Logger.Info("Complete {0} elapsed", stopwatch.Elapsed);
            }
        }

        private async Task<bool> PostBatch(List<Object> content)
        {
            if (content.Count == 0)
            {
                Logger.Debug("No content");
                return true;
            }

            batchCount++;
            HttpResponse result = await Http.PostAsync(Api, content);
            if (result.Is(200))
            {
                Logger.Info("POST batch {0} OK {1} elapsed.", batchCount, result.Elapsed);
                return true;
            }
            else
            {
                var body = result.GetBodyAsString();
                Logger.Error("POST batch {0} failed: {1} - {2}{3}\n{4} elapsed.", batchCount, result.Status, result.Reason, body, result.Elapsed);
                return false;
            }
        }

        protected abstract IQueryable<T> GetEntities(DataBridge databridge);

        protected virtual Object ToRequest(T model)
        {
            return model;
        }

        private void Checkpoint(T model)
        {
            Track(model);
            object currentCheckpointValue = GetCheckpointValue();
            if (!ObjectUtil.AreEqual(priorCheckpointValue, currentCheckpointValue))
            {
                priorCheckpointValue = currentCheckpointValue;
                totalCount = 0;
            }
            totalCount++;
        }

        protected virtual void Track(T model)
        {            
        }        

        protected virtual object GetCheckpointValue()
        {
            return null;
        }

        protected virtual void UpdateCheckpoints(IDictionary<string, string> checkpoints)
        {
            checkpoints[PriorCountProperty] = totalCount.ToString();            
        }
    }
}
