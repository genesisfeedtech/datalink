﻿using System;
using System.Collections.Generic;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class IngredientDefinitionReader : EFReader<FMS_IngredientDefinition>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();
        public const string CheckpointConfig = "Checkpoint_IngredientDefinitionBrillVersionDate";
        new private const String Api = "/api/integration/brill/ingredientDefinition/add";
        public DateTime? LastBrillVersionDate { get; private set; } = null;

        public IngredientDefinitionReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
            LastBrillVersionDate = config.GetDateTime(CheckpointConfig, LastBrillVersionDate);
            if (LastBrillVersionDate.HasValue)
            {
                logger.Info("Resume at {0}", LastBrillVersionDate);
            }
        }

        protected override IQueryable<FMS_IngredientDefinition> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_IngredientDefinition> query = databridge.FMS_IngredientDefinition;
            if (Config.IsRestrictByPlant())
            {
                var plants = Config.GetAllowedPlants();
                query = query.Where(i => plants.Contains(i.PlantCode));
            }
            if (LastBrillVersionDate.HasValue)
            {
                query = query.Where(f => !f.BrillVersionDate.HasValue || f.BrillVersionDate.Value >= LastBrillVersionDate.Value);
            }
            query = query.OrderBy(i => i.BrillVersionDate);
            return query;
        }

        protected override void Track(FMS_IngredientDefinition model)
        {
            base.Track(model);
            this.LastBrillVersionDate = model.BrillVersionDate;
        }

        protected override object GetCheckpointValue()
        {
            return LastBrillVersionDate;
        }

        protected override void UpdateCheckpoints(IDictionary<string, string> checkpoints)
        {
            base.UpdateCheckpoints(checkpoints);
            checkpoints.Add(CheckpointConfig, Config.ToString(LastBrillVersionDate));
        }
    }
}
