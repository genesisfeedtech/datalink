﻿using System;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class SolutionFormulaNutrientReader : EFReader<FMS_SolutionFormulaNutrient>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        new private const String Api = "/api/integration/brill/solutionFormulaNutrient/add";
        private DateTime? LastCreationDate = null;

        public SolutionFormulaNutrientReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
            LastCreationDate = config.GetDateTime(SolutionReader.CheckpointConfig, LastCreationDate);
            if (LastCreationDate.HasValue)
            {
                logger.Info("Resume at {0}", LastCreationDate);
            }
        }

        protected override IQueryable<FMS_SolutionFormulaNutrient> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_SolutionFormulaNutrient> query = databridge.FMS_SolutionFormulaNutrient;
            if (Config.IsRestrictByUser())
            {
                var users = Config.GetAllowedUsers();
                query = query.Where(s => users.Contains(s.SolutionFormula.Solution.UserName));
            }
            if (LastCreationDate.HasValue)
            {
                query = query.Where(n => n.SolutionFormula.Solution.CreationDate >= LastCreationDate.Value);
            }
            query = query.OrderBy(n => n.SolutionFormula.Solution.CreationDate);

            return query;
        }

        protected override object GetCheckpointValue()
        {
            return LastCreationDate;
        }
    }
}
