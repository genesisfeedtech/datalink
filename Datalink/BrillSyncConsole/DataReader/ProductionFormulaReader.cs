﻿using System;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class ProductionFormulaReader : EFReader<FMS_ProductionFormula>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        new private const String Api = "/api/integration/brill/productionFormula/add";

        public ProductionFormulaReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
        }

        protected override IQueryable<FMS_ProductionFormula> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_ProductionFormula> query = databridge.FMS_ProductionFormula;
            if (Config.IsRestrictByPlant())
            {
                var plants = Config.GetAllowedPlants();
                query = query.Where(f => plants.Contains(f.PlantCode));
            }

            return query;
        }
    }
}
