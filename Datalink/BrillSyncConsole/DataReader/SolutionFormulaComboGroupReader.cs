﻿using System;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class SolutionFormulaComboGroupReader : EFReader<FMS_SolutionFormulaComboGroup>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        new private const String Api = "/api/integration/brill/solutionFormulaComboGroup/add";
        private DateTime? LastCreationDate = null;

        public SolutionFormulaComboGroupReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
            LastCreationDate = config.GetDateTime(SolutionReader.CheckpointConfig, LastCreationDate);
            if (LastCreationDate.HasValue)
            {
                logger.Info("Resume at {0}", LastCreationDate);
            }
        }

        protected override IQueryable<FMS_SolutionFormulaComboGroup> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_SolutionFormulaComboGroup> query = databridge.FMS_SolutionFormulaComboGroup;
            if (Config.IsRestrictByUser())
            {
                var users = Config.GetAllowedUsers();
                query = query.Where(s => users.Contains(s.SolutionFormula.Solution.UserName));
            }
            if (LastCreationDate.HasValue)
            {
                query = query.Where(f => f.SolutionFormula.Solution.CreationDate >= LastCreationDate.Value);
            }
            query = query.OrderBy(f => f.SolutionFormula.Solution.CreationDate);

            return query;
        }

        protected override object GetCheckpointValue()
        {
            return LastCreationDate;
        }
    }
}
