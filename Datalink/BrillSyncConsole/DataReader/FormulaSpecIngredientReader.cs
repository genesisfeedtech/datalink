﻿using System;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class FormulaSpecIngredientReader : EFReader<FMS_FormulaSpecIngredient>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();
        new private const String Api = "/api/integration/brill/formulaSpecIngredient/add";

        public FormulaSpecIngredientReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
        }

        protected override IQueryable<FMS_FormulaSpecIngredient> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_FormulaSpecIngredient> query = databridge.FMS_FormulaSpecIngredient;
            if (Config.IsRestrictByPlant())
            {
                var plants = Config.GetAllowedPlants();
                query = query.Where(f => plants.Contains(f.FormulaSpec.PlantCode));
            }

            return query;
        }
    }
}
