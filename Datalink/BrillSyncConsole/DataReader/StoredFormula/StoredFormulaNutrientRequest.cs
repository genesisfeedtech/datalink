﻿using System;
using BrillSyncConsole.DataModel;

namespace BrillSyncConsole.DataReader.StoredFormula
{
    public class StoredFormulaNutrientRequest
    {
        public String formulaId;
        public String code;
        public String description;
        public decimal amount;
        public String units;

        public StoredFormulaNutrientRequest(FMS_StoredFormulaNutrient model)
        {
            formulaId = model.FormulaID.ToString();
            code = model.Code;
            description = model.Description;
            amount = model.Amount;
            units = model.Units;
        }
    }
}
