﻿using System;
using System.Collections.Generic;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader.StoredFormula
{
    class StoredFormulaReader : EFReader<FMS_StoredFormula>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public const string CheckpointConfig = "Checkpoint_StoredFormulaLastLog";
        new private const String Api = "/api/integration/brill/storedFormula/create";
        public DateTime? LastLogDate { get; private set; } = null;

        public StoredFormulaReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
            BatchSize = 10;
            LastLogDate = config.GetDateTime(CheckpointConfig, LastLogDate);
            if (LastLogDate.HasValue)
            {
                logger.Info("Resume at {0}", LastLogDate);
            }
        }

        protected override IQueryable<FMS_StoredFormula> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_StoredFormula> query = databridge.FMS_StoredFormula.Include("Ingredients").Include("Nutrients");
            if (Config.IsRestrictByPlant())
            {
                var plants = Config.GetAllowedPlants();
                query = query.Where(f => plants.Contains(f.PlantCode));
            }
            if (LastLogDate.HasValue)
            {
                query = query.Where(f => !f.LogDate.HasValue || f.LogDate.Value >= LastLogDate.Value);
            }
            query = query.OrderBy(f => f.LogDate);

            return query;
        }

        protected override Object ToRequest(FMS_StoredFormula model)
        {         
            return new StoredFormulaRequest(model);
        }

        protected override void Track(FMS_StoredFormula model)
        {
            base.Track(model);
            this.LastLogDate = model.LogDate;
        }

        protected override object GetCheckpointValue()
        {
            return LastLogDate;
        }

        protected override void UpdateCheckpoints(IDictionary<string, string> checkpoints)
        {
            base.UpdateCheckpoints(checkpoints);
            checkpoints.Add(CheckpointConfig, Config.ToString(LastLogDate));
        }
    }
}
