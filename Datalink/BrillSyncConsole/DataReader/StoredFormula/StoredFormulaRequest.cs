﻿using System;
using System.Collections.Generic;
using BrillSyncConsole.DataModel;

namespace BrillSyncConsole.DataReader.StoredFormula
{
    class StoredFormulaRequest
    {
        public String plantCode;
        public String formulaCode;
        public long versionNumber;
        public String description;
        public decimal batchWeight;
        public decimal costWhenStored;
        public decimal speciesCode;
        public DateTimeOffset? storeDate;
        public String logUser;
        public DateTimeOffset? logDate;
        public String formulaId;
        public String pricingPlant;
        public DateTimeOffset? effectiveDate;
        public DateTimeOffset? expirationDate;
        public String formulaSpecId;
        public String comment;
        public List<StoredFormulaIngredientRequest> ingredients;
        public List<StoredFormulaNutrientRequest> nutrients;

        public StoredFormulaRequest(FMS_StoredFormula model)
        {
            plantCode = model.PlantCode;
            formulaCode = model.FormulaCode;
            versionNumber = model.Version;
            description = model.Description;
            batchWeight = model.BatchWeight;
            costWhenStored = model.CostWhenStored;
            speciesCode = model.SpeciesCode;
            storeDate = model.StoreDate;
            logUser = model.LogUser;
            logDate = model.LogDate;
            formulaId = model.FormulaID.ToString();
            pricingPlant = model.PricingPlant;
            effectiveDate = model.EffectiveDate;
            expirationDate = model.ExpirationDate;
            formulaSpecId = model.FormulaSpecID.ToString();
            comment = model.Comment;
            ingredients = new List<StoredFormulaIngredientRequest>();
            foreach (FMS_StoredFormulaIngredient ingredient in model.Ingredients)
            {
                ingredients.Add(new StoredFormulaIngredientRequest(ingredient));
            }

            nutrients = new List<StoredFormulaNutrientRequest>();
            foreach (FMS_StoredFormulaNutrient nutrient in model.Nutrients)
            {
                nutrients.Add(new StoredFormulaNutrientRequest(nutrient));
            }
        }
    }
}
