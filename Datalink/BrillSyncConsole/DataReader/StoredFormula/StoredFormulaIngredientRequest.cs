﻿using System;
using BrillSyncConsole.DataModel;

namespace BrillSyncConsole.DataReader.StoredFormula
{
    public class StoredFormulaIngredientRequest
    {
        public String formulaId;
        public String code;
        public String description;
        public decimal amount;
        public String batchCode;
        public String groupNumber;
        public String binNumber;
        public decimal cost;
        public decimal costPerUnit;
        public String ingrDefinitionId;
        public String premixFormulaCode;
        public Boolean inStock;

        public StoredFormulaIngredientRequest(FMS_StoredFormulaIngredient model)
        {
            formulaId = model.FormulaID.ToString();
            code = model.Code;
            description = model.Description;
            amount = model.Amount;
            batchCode = model.BatchCode;
            groupNumber = model.GroupNumber;
            binNumber = model.BinNumber;
            cost = model.Cost;
            costPerUnit = model.CostPerUnit;
            ingrDefinitionId = model.IngrDefinitionID.ToString();
            premixFormulaCode = model.PremixFormulaCode;
            inStock = model.InStock.GetValueOrDefault(false);
        }
    }
}
