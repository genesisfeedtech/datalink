﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader.PlantCode
{
    class PlantCodeReader
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private String Query = "SELECT PlantCode FROM FMS_FormulaSpec UNION SELECT PlantCode FROM FMS_IngredientDefinition UNION SELECT PlantCode FROM FMS_IngredientPrices UNION SELECT PlantCode FROM FMS_ProductionFormula UNION SELECT PlantCode FROM FMS_StoredFormula ORDER BY PlantCode";
        private String Api = "/api/integration/brill/integration/addPlants";
        private BrillSyncConfiguration Config;
        private Http http;


        public PlantCodeReader(BrillSyncConfiguration config, Http http)
        {
            this.Config = config;
            this.http = http;
        }

        public async Task ExecuteAsync()
        {
            if (Config == null)
            {
                logger.Error("{0} Abort: Configuration not loaded", GetType());
                return;
            }

            using (SqlConnection sqlConnection = new SqlConnection(Config.GetSqlConnectionString())) {
                SqlCommand cmd = new SqlCommand(Query, sqlConnection);
                sqlConnection.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                List<PlantCodeRequest> codes = new List<PlantCodeRequest>();
                while (reader.Read())
                {
                    codes.Add(ReadRow(reader));
                }

                HttpResponse result = await http.PostAsync(Api, codes);
                if (result.Is(200))
                {
                    logger.Info("PlantCodes POST OK {0} elapsed.", result.Elapsed);
                } else
                {
                    logger.Error("PlantCodes POST failed: {0} - {1}", result.Status, result.Reason);
                }
            }
        }

        private PlantCodeRequest ReadRow(IDataRecord record)
        {
            return new PlantCodeRequest
            {
                code = record.GetString(0)
            };
        }
    }
}
