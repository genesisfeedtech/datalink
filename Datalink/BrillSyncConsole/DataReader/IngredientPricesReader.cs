﻿using System;
using System.Collections.Generic;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class IngredientPricesReader : EFReader<FMS_IngredientPrices>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();
        public const string CheckpointConfig = "Checkpoint_IngredientPricesLastLog";
        new private const String Api = "/api/integration/brill/ingredientPrices/add";
        public DateTime? LastLogDate { get; private set; } = null;

        public IngredientPricesReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
            LastLogDate = config.GetDateTime(CheckpointConfig, LastLogDate);
            if (LastLogDate.HasValue)
            {
                logger.Info("Resume at {0}", LastLogDate);
            }
        }

        protected override IQueryable<FMS_IngredientPrices> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_IngredientPrices> query = databridge.FMS_IngredientPrices;
            if (Config.IsRestrictByPlant())
            {
                var plants = Config.GetAllowedPlants();
                query = query.Where(i => plants.Contains(i.PlantCode));
            }
            if (LastLogDate.HasValue)
            {
                query = query.Where(f => !f.LogDate.HasValue || f.LogDate.Value >= LastLogDate.Value);
            }
            query = query.OrderBy(f => f.LogDate);

            return query;
        }

        protected override void Track(FMS_IngredientPrices model)
        {
            base.Track(model);
            this.LastLogDate = model.LogDate;
        }

        protected override object GetCheckpointValue()
        {
            return LastLogDate;
        }

        protected override void UpdateCheckpoints(IDictionary<string, string> checkpoints)
        {
            base.UpdateCheckpoints(checkpoints);
            checkpoints.Add(CheckpointConfig, Config.ToString(LastLogDate));
        }
    }
}
