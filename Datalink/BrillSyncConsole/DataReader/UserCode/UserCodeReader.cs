﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader.UserCode
{
    class UserCodeReader
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private String Query = "SELECT UserName FROM FMS_Solution UNION SELECT LogUser AS UserName from FMS_IngredientDefinition UNION SELECT LogUser AS UserName from FMS_IngredientPrices UNION SELECT LogUser AS UserName from FMS_StoredFormula ORDER BY UserName;";
        private String Api = "/api/integration/brill/integration/addUsers";
        private BrillSyncConfiguration Config;
        private Http http;

        public UserCodeReader(BrillSyncConfiguration config, Http http)
        {
            this.Config = config;
            this.http = http;
        }

        public async Task ExecuteAsync()
        {
            if (Config == null)
            {
                logger.Error("{0} Abort: Configuration not loaded", GetType());
                return;
            }

            using (SqlConnection sqlConnection = new SqlConnection(Config.GetSqlConnectionString())) {
                SqlCommand cmd = new SqlCommand(Query, sqlConnection);
                sqlConnection.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                List<UserCodeRequest> codes = new List<UserCodeRequest>();
                while (reader.Read())
                {
                    codes.Add(ReadRow(reader));
                }

                HttpResponse result = await http.PostAsync(Api, codes);
                if (result.Is(200))
                {
                    logger.Info("UserCodes POST OK {0} elapsed.", result.Elapsed);
                } else
                {
                    logger.Error("UserCodes POST failed: {0} - {1}", result.Status, result.Reason);
                }
            }
        }

        private UserCodeRequest ReadRow(IDataRecord record)
        {
            return new UserCodeRequest
            {
                code = record.GetString(0)
            };
        }
    }
}
