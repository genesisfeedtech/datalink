﻿using System;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class IngredientNutrientReader : EFReader<FMS_IngredientNutrient>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        new private const String Api = "/api/integration/brill/ingredientNutrient/add";
        public DateTime? LastBrillVersionDate { get; private set; } = null;

        public IngredientNutrientReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
            LastBrillVersionDate = config.GetDateTime(IngredientDefinitionReader.CheckpointConfig, LastBrillVersionDate);
            if (LastBrillVersionDate.HasValue)
            {
                logger.Info("Resume at {0}", LastBrillVersionDate);
            }
        }

        protected override IQueryable<FMS_IngredientNutrient> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_IngredientNutrient> query = databridge.FMS_IngredientNutrient;
            if (Config.IsRestrictByPlant())
            {
                var plants = Config.GetAllowedPlants();
                query = query.Where(i => plants.Contains(i.IngredientDefinition.PlantCode));
            }
            if (LastBrillVersionDate.HasValue)
            {
                query = query.Where(f => !f.IngredientDefinition.BrillVersionDate.HasValue || f.IngredientDefinition.BrillVersionDate.Value >= LastBrillVersionDate.Value);
            }

            return query;
        }

        protected override object GetCheckpointValue()
        {
            return LastBrillVersionDate;
        }

    }
}
