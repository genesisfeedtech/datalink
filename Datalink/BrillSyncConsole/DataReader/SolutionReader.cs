﻿using System;
using System.Collections.Generic;
using System.Linq;
using BrillSyncConsole.DataModel;
using Common;
using NLog;

namespace BrillSyncConsole.DataReader
{
    class SolutionReader : EFReader<FMS_Solution>
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        public const string CheckpointConfig = "Checkpoint_SolutionLastLog";
        new private const String Api = "/api/integration/brill/solution/add";
        public DateTime? LastCreationDate { get; private set; } = null;

        public SolutionReader(BrillSyncConfiguration config, Http http) : base(config, http, Api, logger)
        {
            LastCreationDate = config.GetDateTime(CheckpointConfig, LastCreationDate);
            if (LastCreationDate.HasValue)
            {
                logger.Info("Resume at {1}", LastCreationDate);
            }
        }

        protected override IQueryable<FMS_Solution> GetEntities(DataBridge databridge)
        {
            IQueryable<FMS_Solution> query = databridge.FMS_Solution;
            if (Config.IsRestrictByUser())
            {
                var users = Config.GetAllowedUsers();
                query = query.Where(s => users.Contains(s.UserName));
            }
            if (LastCreationDate.HasValue)
            {
                query = query.Where(f => f.CreationDate >= LastCreationDate.Value);
            }
            query = query.OrderBy(f => f.CreationDate);

            return query;
        }

        protected override void Track(FMS_Solution model)
        {
            base.Track(model);
            this.LastCreationDate = model.CreationDate;
        }

        protected override object GetCheckpointValue()
        {
            return LastCreationDate;
        }

        protected override void UpdateCheckpoints(IDictionary<string, string> checkpoints)
        {
            base.UpdateCheckpoints(checkpoints);
            checkpoints.Add(CheckpointConfig, Config.ToString(LastCreationDate));
        }
    }
}
