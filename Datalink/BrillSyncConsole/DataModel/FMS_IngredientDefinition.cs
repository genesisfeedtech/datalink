using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_IngredientDefinition
    {
        [Key]
        [JsonProperty(PropertyName = "ingrDefinitionId")]
        public Guid IngrDefinitionID { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "plantCode")]
        public string PlantCode { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "ingredientCode")]
        public string IngredientCode { get; set; }

        [Required]
        [StringLength(100)]
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [Required]
        [StringLength(50)]
        [JsonProperty(PropertyName = "premixFormulaCode")]
        public string PremixFormulaCode { get; set; }

        [JsonProperty(PropertyName = "productionMinimum")]
        public decimal ProductionMinimum { get; set; }

        [JsonProperty(PropertyName = "roundingFactor")]
        public decimal RoundingFactor { get; set; }

        [JsonProperty(PropertyName = "formulaInclude")]
        public bool FormulaInclude { get; set; }

        [JsonProperty(PropertyName = "brillVersionDate")]
        public DateTime? BrillVersionDate { get; set; }

        [JsonProperty(PropertyName = "brillVersionNumber")]
        public int BrillVersionNumber { get; set; }

        [JsonProperty(PropertyName = "logDate")]
        public DateTime? LogDate { get; set; }

        [StringLength(20)]
        [JsonProperty(PropertyName = "logUser")]
        public string LogUser { get; set; }

        [Required]
        [StringLength(100)]
        [JsonProperty(PropertyName = "hashValue")]
        public string HashValue { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [JsonProperty(PropertyName = "versionNumber")]
        public int VersionNumber { get; set; }

        [JsonIgnore]
        [InverseProperty("IngredientDefinition")]
        public virtual ICollection<FMS_IngredientNutrient> Nutrients { get; set; }
    }
}
