using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_IngredientPrices
    {
        [JsonProperty(PropertyName = "archivedPriceId")]
        public Guid ArchivedPriceID { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        [JsonProperty(PropertyName = "plantCode")]
        public string PlantCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        [JsonProperty(PropertyName = "ingredientCode")]
        public string IngredientCode { get; set; }

        [JsonProperty(PropertyName = "cost")]
        public decimal Cost { get; set; }

        [JsonProperty(PropertyName = "costPerUnit")]
        public decimal CostPerUnit { get; set; }

        [JsonProperty(PropertyName = "inStock")]
        public bool InStock { get; set; }

        [JsonProperty(PropertyName = "logDate")]
        public DateTime? LogDate { get; set; }

        [Required]
        [StringLength(20)]
        [JsonProperty(PropertyName = "logUser")]
        public string LogUser { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [JsonProperty(PropertyName = "versionNumber")]
        public int VersionNumber { get; set; }
    }
}
