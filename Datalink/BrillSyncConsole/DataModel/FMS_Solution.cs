using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_Solution
    {
        [Key]
        [JsonProperty(PropertyName = "solutionId")]
        public Guid SolutionID { get; set; }

        [StringLength(255)]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [JsonProperty(PropertyName = "versionNumber")]
        public int Version { get; set; }

        [Required]
        [StringLength(50)]
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100)]
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "creationDate")]
        public DateTime CreationDate { get; set; }

        [JsonIgnore]
        [InverseProperty("Solution")]
        public virtual ICollection<FMS_SolutionFormula> Formulas { get; set; }

        [JsonIgnore]
        [InverseProperty("Solution")]
        public virtual ICollection<FMS_SolutionPlant> Plants { get; set; }

        [JsonIgnore]
        [InverseProperty("Solution")]
        public virtual ICollection<FMS_SolutionPlantGroup> PlantGroups { get; set; }

        [JsonIgnore]
        [InverseProperty("Solution")]
        public virtual ICollection<FMS_SolutionGlobal> Globals { get; set; }

        [JsonIgnore]
        [InverseProperty("Solution")]
        public virtual ICollection<FMS_SolutionGlobalGroup> GlobalGroups { get; set; }
    }
}
