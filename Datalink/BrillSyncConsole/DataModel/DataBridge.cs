using System;
using System.Data.Entity;

namespace BrillSyncConsole.DataModel
{
    public partial class DataBridge : DbContext
    {
        public DataBridge(String SqlConnectionString)
            : base(SqlConnectionString)
        {
        }

        public virtual DbSet<FMS_db_info> FMS_db_info { get; set; }
        public virtual DbSet<FMS_FormulaSpec> FMS_FormulaSpec { get; set; }
        public virtual DbSet<FMS_FormulaSpecIngredient> FMS_FormulaSpecIngredient { get; set; }
        public virtual DbSet<FMS_FormulaSpecNutrient> FMS_FormulaSpecNutrient { get; set; }
        public virtual DbSet<FMS_IngredientDefinition> FMS_IngredientDefinition { get; set; }
        public virtual DbSet<FMS_IngredientNutrient> FMS_IngredientNutrient { get; set; }
        public virtual DbSet<FMS_IngredientPrices> FMS_IngredientPrices { get; set; }
        public virtual DbSet<FMS_ProductionFormula> FMS_ProductionFormula { get; set; }
        public virtual DbSet<FMS_Solution> FMS_Solution { get; set; }
        public virtual DbSet<FMS_SolutionFormula> FMS_SolutionFormula { get; set; }
        public virtual DbSet<FMS_SolutionFormulaComboGroup> FMS_SolutionFormulaComboGroup { get; set; }
        public virtual DbSet<FMS_SolutionFormulaIngredient> FMS_SolutionFormulaIngredient { get; set; }
        public virtual DbSet<FMS_SolutionFormulaNutrient> FMS_SolutionFormulaNutrient { get; set; }
        public virtual DbSet<FMS_SolutionGlobal> FMS_SolutionGlobal { get; set; }
        public virtual DbSet<FMS_SolutionGlobalGroup> FMS_SolutionGlobalGroup { get; set; }
        public virtual DbSet<FMS_SolutionPlant> FMS_SolutionPlant { get; set; }
        public virtual DbSet<FMS_SolutionPlantGroup> FMS_SolutionPlantGroup { get; set; }
        public virtual DbSet<FMS_StoredFormula> FMS_StoredFormula { get; set; }
        public virtual DbSet<FMS_StoredFormulaIngredient> FMS_StoredFormulaIngredient { get; set; }
        public virtual DbSet<FMS_StoredFormulaNutrient> FMS_StoredFormulaNutrient { get; set; }
    }
}
