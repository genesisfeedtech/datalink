using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_SolutionFormulaNutrient
    {
        [Key]
        [Column(Order = 0)]
        [JsonProperty(PropertyName = "formulaId")]
        [ForeignKey("SolutionFormula")]
        public Guid FormulaID { get; set; }

        [JsonIgnore]
        public virtual FMS_SolutionFormula SolutionFormula { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [Required]
        [StringLength(100)]
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName = "unroundedAmount")]
        public decimal UnroundedAmount { get; set; }

        [JsonProperty(PropertyName = "storedAmount")]
        public decimal StoredAmount { get; set; }

        [JsonProperty(PropertyName = "minimum")]
        public decimal Minimum { get; set; }

        [JsonProperty(PropertyName = "maximum")]
        public decimal Maximum { get; set; }

        [JsonProperty(PropertyName = "lowCost")]
        public decimal LowCost { get; set; }

        [JsonProperty(PropertyName = "lowAmount")]
        public decimal LowAmount { get; set; }

        [JsonProperty(PropertyName = "highCost")]
        public decimal HighCost { get; set; }

        [JsonProperty(PropertyName = "highAmount")]
        public decimal HighAmount { get; set; }

        [JsonProperty(PropertyName = "restrictionCost")]
        public decimal RestrictionCost { get; set; }

        [JsonProperty(PropertyName = "extraValue")]
        public decimal ExtraValue { get; set; }

        [JsonProperty(PropertyName = "extra")]
        public decimal Extra { get; set; }

        [JsonProperty(PropertyName = "savingsValue")]
        public decimal SavingsValue { get; set; }

        [JsonProperty(PropertyName = "savings")]
        public decimal Savings { get; set; }
    }
}
