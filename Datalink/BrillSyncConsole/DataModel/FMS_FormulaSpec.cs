using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_FormulaSpec
    {
        [Key]
        [JsonProperty(PropertyName = "formulaSpecId")]
        public Guid FormulaSpecID { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "plantCode")]
        public string PlantCode { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "formulaCode")]
        public string FormulaCode { get; set; }

        [Required]
        [StringLength(100)]
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "specWeight")]
        public decimal SpecWeight { get; set; }

        [JsonProperty(PropertyName = "dryMatter")]
        public decimal DryMatter { get; set; }

        [JsonProperty(PropertyName = "energy1")]
        public decimal Energy1 { get; set; }

        [JsonProperty(PropertyName = "energy2")]
        public decimal Energy2 { get; set; }

        [JsonProperty(PropertyName = "speciesCode")]
        public int SpeciesCode { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [JsonProperty(PropertyName = "versionNumber")]
        public int Version { get; set; }

        [Required]
        [StringLength(100)]
        [JsonProperty(PropertyName = "hashValue")]
        public string HashValue { get; set; }

        [JsonIgnore]
        [InverseProperty("FormulaSpec")]
        public virtual ICollection<FMS_FormulaSpecIngredient> Ingredients { get; set; }

        [JsonIgnore]
        [InverseProperty("FormulaSpec")]
        public virtual ICollection<FMS_FormulaSpecNutrient> Nutrients { get; set; }
    }
}
