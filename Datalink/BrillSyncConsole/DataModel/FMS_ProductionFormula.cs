using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_ProductionFormula
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        [JsonProperty(PropertyName = "plantCode")]
        public string PlantCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        [JsonProperty(PropertyName = "formulaCode")]
        public string FormulaCode { get; set; }

        [JsonProperty(PropertyName = "formulaId")]
        public Guid FormulaID { get; set; }
    }
}
