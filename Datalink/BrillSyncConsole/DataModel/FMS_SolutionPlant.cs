using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_SolutionPlant
    {
        [Key]
        [Column(Order = 0)]
        [JsonProperty(PropertyName = "solutionId")]
        [ForeignKey("Solution")]
        public Guid SolutionID { get; set; }

        [JsonIgnore]
        public virtual FMS_Solution Solution { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        [JsonProperty(PropertyName = "plant")]
        public string Plant { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        [JsonProperty(PropertyName = "instanceCode")]
        public string InstanceCode { get; set; }

        [Required]
        [StringLength(100)]
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName = "unroundedAmount")]
        public decimal UnroundedAmount { get; set; }

        [JsonProperty(PropertyName = "storedAmount")]
        public decimal StoredAmount { get; set; }

        [JsonProperty(PropertyName = "cost")]
        public decimal Cost { get; set; }

        [JsonProperty(PropertyName = "inStock")]
        public bool InStock { get; set; }

        [JsonProperty(PropertyName = "minimum")]
        public decimal Minimum { get; set; }

        [JsonProperty(PropertyName = "maximum")]
        public decimal Maximum { get; set; }

        [JsonProperty(PropertyName = "lowCost")]
        public decimal LowCost { get; set; }

        [JsonProperty(PropertyName = "lowAmount")]
        public decimal LowAmount { get; set; }

        [JsonProperty(PropertyName = "highCost")]
        public decimal HighCost { get; set; }

        [JsonProperty(PropertyName = "highAmount")]
        public decimal HighAmount { get; set; }

        [JsonProperty(PropertyName = "restrictionCost")]
        public decimal RestrictionCost { get; set; }

        [JsonProperty(PropertyName = "extraValue")]
        public decimal ExtraValue { get; set; }

        [JsonProperty(PropertyName = "extra")]
        public decimal Extra { get; set; }

        [JsonProperty(PropertyName = "savingsValue")]
        public decimal SavingsValue { get; set; }

        [JsonProperty(PropertyName = "savings")]
        public decimal Savings { get; set; }
    }
}
