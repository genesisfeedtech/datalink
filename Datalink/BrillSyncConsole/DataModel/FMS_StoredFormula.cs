using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_StoredFormula
    {
        // FormulaID is the identifier used in the foreign key relationship between the stored formula and its ingredients/nutrients
        // The underlying database is using PlantCode, FormulaCode, Version as a composite PK for FMS_StoredFormula.  
        // Entity Framework doesn't want to join the collection properties using a non-PK foreign key.
        // We are not writing to the database, so this should be ok
        [Key]
        public Guid FormulaID { get; set; }

        //[Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string PlantCode { get; set; }

       // [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string FormulaCode { get; set; }

        //[Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Version { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public decimal BatchWeight { get; set; }

        public decimal CostWhenStored { get; set; }

        public short SpeciesCode { get; set; }

        public DateTime? StoreDate { get; set; }

        [Required]
        [StringLength(20)]
        public string LogUser { get; set; }

        public DateTime? LogDate { get; set; }

        [Required]
        [StringLength(50)]
        public string PricingPlant { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public Guid? FormulaSpecID { get; set; }

        [StringLength(1000)]
        public string Comment { get; set; }

        [InverseProperty("StoredFormula")]
        public ICollection<FMS_StoredFormulaIngredient> Ingredients { get; set; }

        [InverseProperty("StoredFormula")]
        public ICollection<FMS_StoredFormulaNutrient> Nutrients { get; set; }

    }
}
