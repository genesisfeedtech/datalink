using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_FormulaSpecNutrient
    {
        [Key]
        [Column(Order = 0)]
        [JsonProperty(PropertyName = "formulaSpecId")]
        [ForeignKey("FormulaSpec")]
        public Guid FormulaSpecID { get; set; }

        [JsonIgnore]
        public virtual FMS_FormulaSpec FormulaSpec { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [Required]
        [StringLength(100)]
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [Required]
        [StringLength(50)]
        [JsonProperty(PropertyName = "units")]
        public string Units { get; set; }

        [JsonProperty(PropertyName = "minimum")]
        public decimal Minimum { get; set; }

        [JsonProperty(PropertyName = "maximum")]
        public decimal Maximum { get; set; }
    }
}
