using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_IngredientNutrient
    {
        [Key]
        [Column(Order = 0)]
        [JsonProperty(PropertyName = "ingrDefinitionId")]
        [ForeignKey("IngredientDefinition")]
        public Guid IngrDefinitionID { get; set; }

        [JsonIgnore]
        public virtual FMS_IngredientDefinition IngredientDefinition { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        [JsonProperty(PropertyName = "nutrientCode")]
        public string NutrientCode { get; set; }

        [Required]
        [StringLength(100)]
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [Required]
        [StringLength(50)]
        [JsonProperty(PropertyName = "units")]
        public string Units { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [JsonProperty(PropertyName = "meanValue")]
        public decimal MeanValue { get; set; }

        [JsonProperty(PropertyName = "stdValue")]
        public decimal STDValue { get; set; }
    }
}
