using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_SolutionFormula
    {
        [Key]
        [JsonProperty(PropertyName = "formulaId")]
        public Guid FormulaID { get; set; }

        [JsonProperty(PropertyName = "solutionId")]
        [ForeignKey("Solution")]
        public Guid SolutionID { get; set; }

        [JsonIgnore]
        public virtual FMS_Solution Solution { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "plant")]
        public string Plant { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [Required]
        [StringLength(100)]
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "cost")]
        public decimal Cost { get; set; }

        [JsonProperty(PropertyName = "unroundedCost")]
        public decimal UnroundedCost { get; set; }

        [JsonProperty(PropertyName = "batchSize")]
        public decimal BatchSize { get; set; }

        [JsonProperty(PropertyName = "unroundedBatchSize")]
        public decimal UnroundedBatchSize { get; set; }

        [JsonProperty(PropertyName = "specBatch")]
        public decimal SpecBatch { get; set; }

        [JsonProperty(PropertyName = "storeBatch")]
        public decimal StoreBatch { get; set; }

        [JsonProperty(PropertyName = "storeCost")]
        public decimal StoreCost { get; set; }

        [JsonProperty(PropertyName = "costWhenStored")]
        public decimal CostWhenStored { get; set; }

        [JsonProperty(PropertyName = "dryMatterValue")]
        public decimal DryMatterValue { get; set; }

        [JsonProperty(PropertyName = "minimum")]
        public decimal Minimum { get; set; }

        [JsonProperty(PropertyName = "maximum")]
        public decimal Maximum { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }

        [StringLength(1000)]
        [JsonProperty(PropertyName = "comment")]
        public string Comment { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "specPlant")]
        public string SpecPlant { get; set; }

        [StringLength(50)]
        [JsonProperty(PropertyName = "pricePlant")]
        public string PricePlant { get; set; }

        [JsonProperty(PropertyName = "formulaIsFixed")]
        public bool FormulaIsFixed { get; set; }

        [JsonIgnore]
        [InverseProperty("SolutionFormula")]
        public virtual ICollection<FMS_SolutionFormulaComboGroup> ComboGroups { get; set; }

        [JsonIgnore]
        [InverseProperty("SolutionFormula")]
        public virtual ICollection<FMS_SolutionFormulaIngredient> Ingredients { get; set; }

        [JsonIgnore]
        [InverseProperty("SolutionFormula")]
        public virtual ICollection<FMS_SolutionFormulaNutrient> Nutrients { get; set; }
    }
}
