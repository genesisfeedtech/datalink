using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BrillSyncConsole.DataModel
{
    public partial class FMS_StoredFormulaNutrient
    {
        [Key]
        [Column(Order = 0)]
        [ForeignKey("StoredFormula")]
        public Guid FormulaID { get; set; }

        [JsonIgnore]
        public virtual FMS_StoredFormula StoredFormula { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string Code { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public decimal Amount { get; set; }

        [Required]
        [StringLength(50)]
        public string Units { get; set; }
    }
}
