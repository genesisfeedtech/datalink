﻿namespace BrillSyncService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BrillSyncProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.BrillSyncServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // BrillSyncProcessInstaller
            // 
            this.BrillSyncProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.BrillSyncProcessInstaller.Password = null;
            this.BrillSyncProcessInstaller.Username = null;
            this.BrillSyncProcessInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceProcessInstaller1_AfterInstall);
            // 
            // BrillSyncServiceInstaller
            // 
            this.BrillSyncServiceInstaller.Description = "Genesis Feed Technologies Datalink for Brill";
            this.BrillSyncServiceInstaller.DisplayName = "GFT Datalink - Brill";
            this.BrillSyncServiceInstaller.ServiceName = "GFT Brill Datalink";
            this.BrillSyncServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.BrillSyncServiceInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceInstaller1_AfterInstall);
            this.BrillSyncServiceInstaller.BeforeInstall += new System.Configuration.Install.InstallEventHandler(this.serviceInstaller1_BeforeInstall);
            this.BrillSyncServiceInstaller.BeforeUninstall += new System.Configuration.Install.InstallEventHandler(this.serviceInstaller1_BeforeUninstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.BrillSyncProcessInstaller,
            this.BrillSyncServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller BrillSyncProcessInstaller;
        private System.ServiceProcess.ServiceInstaller BrillSyncServiceInstaller;
    }
}