﻿using System;
using System.ServiceProcess;
using BrillSyncConsole;
using NLog;

namespace BrillSyncService
{
    public class Service : ServiceBase
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly Coordinator coordinator;
        public Service()
        {
            InitializeComponent();
            coordinator = new Coordinator();
        }

        protected override void OnStart(string[] args)
        {
            logger.Info("Service.OnStart");
            try
            {                               
                coordinator.Start();
                
                logger.Info("Service.OnStart success");
            }
            catch (Exception e)
            {
                logger.Error(e, "Service.OnStart exception thrown");
            }
        }

        protected override void OnStop()
        {
        }

        private void InitializeComponent()
        {
            logger.Debug("Service.InitializeComponent");
            // 
            // Service
            // 
            this.ServiceName = "GFT Brill Datalink";            

        }
    }
}
