﻿using System.ServiceProcess;
using Common.Logging;
using NLog;

namespace BrillSyncService
{
    static class Program
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            new Logging().Init();

            logger.Info("Program.main");
               
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
