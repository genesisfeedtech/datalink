﻿using System;
using System.ComponentModel;
using System.Configuration.Install;
using Common.Installer;

namespace BrillSyncService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        //Stop the existing service
        //https://www.codeproject.com/Tips/575177/Window-Service-Deployment-using-VS
        //Remove the existing service
        //https://stackoverflow.com/questions/18371096/error-1001-the-specified-service-already-exists-existing-service-cant-removed
        //General install commentary
        //https://social.msdn.microsoft.com/Forums/windows/en-US/b2d1bd22-8499-454e-9cec-1e42c03e2557/how-to-deal-with-quoterror-1001-the-specified-service-already-existsquot-when-install-a-service
        private void serviceInstaller1_BeforeInstall(object sender, InstallEventArgs e)
        {
            try
            {
                ServiceHelper helper = new ServiceHelper(BrillSyncServiceInstaller.ServiceName);
                helper.StopService();                
            }
            catch (Exception ex)
            {
                throw new System.Configuration.Install.InstallException(ex.Message.ToString());
            }
        }

        private void serviceInstaller1_BeforeUninstall(object sender, InstallEventArgs e)
        {
            try
            {
                ServiceHelper helper = new ServiceHelper(BrillSyncServiceInstaller.ServiceName);
                helper.StopService();
            }
            catch (Exception ex)
            {
                throw new System.Configuration.Install.InstallException(ex.Message.ToString());
            }
        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
            ServiceHelper helper = new ServiceHelper(BrillSyncServiceInstaller.ServiceName);
            helper.StartService();               
        }

        private void serviceProcessInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {

        }
    }
}
