namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5IngrPlantCost
    {
        public int C5IngrPlantCostID { get; set; }

        [StringLength(8)]
        public string PlantCode { get; set; }

        [StringLength(8)]
        public string IngrCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? OwnCost { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MktCost { get; set; }
    }
}
