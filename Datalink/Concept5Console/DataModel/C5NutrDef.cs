namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5NutrDef
    {
        public int C5NutrDefID { get; set; }

        public int? NutrNumber { get; set; }

        [StringLength(20)]
        public string NutrName { get; set; }

        [StringLength(8)]
        public string NutrUnits { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? NutrIncr { get; set; }

        [StringLength(1)]
        public string InClass1 { get; set; }

        [StringLength(1)]
        public string InClass2 { get; set; }

        [StringLength(1)]
        public string InClass3 { get; set; }

        [StringLength(1)]
        public string InClass4 { get; set; }

        [StringLength(1)]
        public string InClass5 { get; set; }

        [StringLength(1)]
        public string InClass6 { get; set; }

        [StringLength(1)]
        public string InClass7 { get; set; }

        [StringLength(1)]
        public string InClass8 { get; set; }

        [StringLength(1)]
        public string InClass9 { get; set; }

        [StringLength(1)]
        public string InClass10 { get; set; }

        [StringLength(1)]
        public string InClass11 { get; set; }

        [StringLength(1)]
        public string InClass12 { get; set; }

        [StringLength(1)]
        public string InClass13 { get; set; }

        [StringLength(1)]
        public string InClass14 { get; set; }

        [StringLength(1)]
        public string InClass15 { get; set; }

        [StringLength(1)]
        public string HasGNEQ { get; set; }

        [StringLength(1)]
        public string AdjustToDM { get; set; }

        [StringLength(1)]
        public string CompInForm { get; set; }
    }
}
