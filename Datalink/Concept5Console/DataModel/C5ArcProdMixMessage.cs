namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Product Mix Message Assignment
    /// </summary>
    public partial class C5ArcProdMixMessage
    {
        //public int C5ProdMixMessageID { get; set; }
       
        /// <summary>
        /// Plant Code
        /// </summary>        
        [Key, Column(Order = 0)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        /// <summary>
        /// Product Code
        /// </summary>
        [Key, Column(Order = 1)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Message Location (1-10)
        /// </summary>
        public int? MessageLoc { get; set; }

        /// <summary>
        /// Message Number
        /// </summary>
        public int? MessageNumber { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdDef C5ArcProdDef { get; set; }
    }
}
