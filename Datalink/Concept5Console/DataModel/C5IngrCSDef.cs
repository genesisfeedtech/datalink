namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5IngrCSDef
    {
        public int C5IngrCSDefID { get; set; }

        [StringLength(8)]
        public string CSCode { get; set; }

        [StringLength(40)]
        public string CSDesc { get; set; }
    }
}
