namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Product Specification Ingredient Type Requirements
    /// </summary>
    public partial class C5ArcProdSpecIType
    {
        //public int C5ProdSpecITypeID { get; set; }

        /// <summary>
        /// Plant Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        /// <summary>
        /// Product Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Ingredient Type Number
        /// </summary>
        public int? TypeNumber { get; set; }

        /// <summary>
        /// Type Minimum
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? TypeMin { get; set; }

        /// <summary>
        /// Type Maximum
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? TypeMax { get; set; }

        /// <summary>
        /// Type Restriction Cost
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? TypeRestCost { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdSpecDef C5ArcProdSpecDef { get; set; }
    }
}
