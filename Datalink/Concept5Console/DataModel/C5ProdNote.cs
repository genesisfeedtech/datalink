namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5ProdNote
    {
        public int C5ProdNoteID { get; set; }

        [StringLength(8)]
        public string PlantCode { get; set; }

        [StringLength(8)]
        public string ProdCode { get; set; }

        [StringLength(10)]
        public string NoteDate { get; set; }

        [StringLength(8)]
        public string NoteTime { get; set; }

        [StringLength(8)]
        public string NoteUserID { get; set; }

        [StringLength(400)]
        public string NoteText { get; set; }
    }
}
