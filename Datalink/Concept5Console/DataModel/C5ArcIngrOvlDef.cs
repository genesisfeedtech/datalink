namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Ingredient Overlay Definition
    /// </summary>
    public partial class C5ArcIngrOvlDef
    {
        //public int C5IngrOvlDefID { get; set; }

        /// <summary>
        /// Plant Code
        /// </summary>
        [StringLength(8)]
        public string PlantCode { get; set; }

        /// <summary>
        /// Ingredient Code
        /// </summary>
        [StringLength(10)]
        public string IngrCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [StringLength(8)]
        public string ArcVersion { get; set; }

        [StringLength(1)]
        public string OvlName { get; set; }

        [StringLength(40)]
        public string Name { get; set; }

        [StringLength(1)]
        public string OvlShortName { get; set; }

        [StringLength(16)]
        public string ShortName { get; set; }

        [StringLength(1)]
        public string OvlAltCode { get; set; }

        [StringLength(8)]
        public string AltCode { get; set; }

        [StringLength(1)]
        public string OvlPICode { get; set; }

        [StringLength(8)]
        public string PICode { get; set; }

        [StringLength(1)]
        public string OvlMoisture { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Moisture { get; set; }

        [StringLength(1)]
        public string OvlRoundAmt { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RoundAmt { get; set; }

        [StringLength(1)]
        public string OvlProdMin { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ProdMin { get; set; }

        [StringLength(1)]
        public string OvlMixScale { get; set; }

        public int? MixScale { get; set; }

        [StringLength(1)]
        public string OvlMixBin { get; set; }

        public int? MixBin { get; set; }

        [StringLength(1)]
        public string OvlMixSeq { get; set; }

        public int? MixSeq { get; set; }

        [StringLength(1)]
        public string OvlMixXref { get; set; }

        public int? MixXref { get; set; }

        [ForeignKey("IngrCode, ArcVersion")]
        public virtual C5ArcIngrDef C5ArcIngrDef { get; set; }

        public virtual ICollection<C5ArcIngrOvlNutrVal> C5ArcIngrOvlNutrVals { get; set; }

    }
}
