namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Production Formula Nutrient Profile
    /// </summary>
    public partial class C5ArcProdPFNutr
    {
        //public int C5ProdProdnFormNutrID { get; set; }

        /// <summary>
        /// Plant Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        /// <summary>
        /// Product Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Nutrient Number
        /// </summary>
        public int? NutrNumber { get; set; }

        /// <summary>
        /// Nutrient Amount
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? NutrAmount { get; set; }

        //[Column(TypeName = "numeric")]
        //public decimal? NutrRoundAmount { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdPFDef C5ArcProdPFDef { get; set; }
    }
}
