namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Ingredient In Types
    /// </summary>
    public partial class C5ArcIngrInType
    {
        //public int C5IngrInTypeID { get; set; }

        /// <summary>
        /// Ingredient Code
        /// </summary>
        [StringLength(10)]
        public string IngrCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Type Number (1 - 100)
        /// </summary>
        public int? TypeNumber { get; set; }
    }
}
