namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Production Formula Ingredient Amounts
    /// </summary>
    public partial class C5ArcProdPFItem
    {
        //public int C5ProdProdnFormItemID { get; set; }

        /// <summary>
        /// Plant Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        /// <summary>
        /// Product Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Ingredient Code
        /// </summary>
        [StringLength(8)]
        public string IngrCode { get; set; }

        /// <summary>
        /// Formula Amount (Fractional part of batch)
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? FormAmount { get; set; }

        //        [Column(TypeName = "numeric")]
        //        public decimal? RoundPounds { get; set; }

        //        [Column(TypeName = "numeric")]
        //        public decimal? Pounds { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdPFDef C5ArcProdPFDef { get; set; }
    }
}
