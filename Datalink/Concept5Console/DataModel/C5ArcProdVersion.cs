namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Product Version Definition
    /// </summary>
    public partial class C5ArcProdVersion
    {
        /// <summary>
        /// Plant Code
        /// </summary>
        [Key, Column(Order = 0)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        /// <summary>
        /// Product Code
        /// </summary>
        [Key, Column(Order = 1)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        //todo verify this column datatype
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Date Archived mm/dd/yyyy
        /// </summary>
        [StringLength(10)]
        public string ArcDate{ get; set; }

        /// <summary>
        /// Time Archived hh:mm:ss       
        /// </summary>
        [StringLength(8)]
        public string ArcTime { get; set; }

        /// <summary>
        /// Archived by Concept5 User ID
        /// </summary>
        [StringLength(8)]
        public string ArcUserID { get; set; }

        /// <summary>
        /// Has Archived Specs (Y or N)
        /// </summary>
        [StringLength(1)]
        public string HasProdSpecs { get; set; }

        /// <summary>
        /// Has Archived Cost Per Ton (Y or N)
        /// </summary>
        [StringLength(1)]
        public string HasPFCpt { get; set; }

        /// <summary>
        /// Cost per Ton when archived
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? PFCpt { get; set; }

        /// <summary>
        /// Has Archived Nutrient Profile (Y or N)
        /// </summary>
        [StringLength(1)]
        public string HasPFNuts { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdSpecDef C5ArcProdSpecDef { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdDef C5ArcProdDef { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdTFDef C5ArcProdTFDef { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdPFDef C5ArcProdPFDef { get; set; }
    }
}
