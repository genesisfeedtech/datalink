namespace Concept5Console.DataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DataModel : DbContext
    {
        public DataModel()
            : base("name=DataModel")
        {
        }

        
        public virtual DbSet<C5IngrCSCost> C5IngrCSCost { get; set; }
        public virtual DbSet<C5IngrCSDef> C5IngrCSDef { get; set; }
        public virtual DbSet<C5ArcIngrDef> C5IngrDef { get; set; }
        public virtual DbSet<C5ArcIngrInType> C5IngrInType { get; set; }
        public virtual DbSet<C5IngrNeq> C5IngrNeq { get; set; }
        public virtual DbSet<C5IngrNote> C5IngrNote { get; set; }
        public virtual DbSet<C5ArcIngrNutVal> C5IngrNutrVal { get; set; }
        public virtual DbSet<C5ArcIngrOvlDef> C5IngrOvlDef { get; set; }
        public virtual DbSet<C5ArcIngrOvlNutrVal> C5IngrOvlNutrVal { get; set; }
        public virtual DbSet<C5IngrPlantCost> C5IngrPlantCost { get; set; }
        public virtual DbSet<C5IngrPltStockStatus> C5IngrPltStockStatus { get; set; }
        public virtual DbSet<C5IngrStringDef> C5IngrStringDef { get; set; }
        public virtual DbSet<C5IngrStringItem> C5IngrStringItem { get; set; }
        public virtual DbSet<C5IngrTypeDef> C5IngrTypeDef { get; set; }
        public virtual DbSet<C5ArcIngrTypePct> C5IngrTypePct { get; set; }        
        public virtual DbSet<C5NutrDef> C5NutrDef { get; set; }
        public virtual DbSet<C5NutrGlobEq> C5NutrGlobEq { get; set; }        
        public virtual DbSet<C5PlantMixMessage> C5PlantMixMessage { get; set; }
        public virtual DbSet<C5ArcProdDef> C5ArcProdDef { get; set; }
        public virtual DbSet<C5ArcProdInType> C5ProdInType { get; set; }
        public virtual DbSet<C5ArcProdMixMessage> C5ProdMixMessage { get; set; }
        public virtual DbSet<C5ProdNote> C5ProdNote { get; set; }
        public virtual DbSet<C5ArcProdPFDef> C5ProdProdnFormDef { get; set; }
        public virtual DbSet<C5ArcProdPFItem> C5ProdProdnFormItem { get; set; }
        public virtual DbSet<C5ArcProdPFNutr> C5ProdProdnFormNutr { get; set; }
        public virtual DbSet<C5ArcProdSpecDef> C5ProdSpecDef { get; set; }
        public virtual DbSet<C5ArcProdSpecIngr> C5ProdSpecIngr { get; set; }
        public virtual DbSet<C5ArcProdSpecIRat> C5ProdSpecIRat { get; set; }
        public virtual DbSet<C5ArcProdSpecIType> C5ProdSpecIType { get; set; }
        public virtual DbSet<C5ArcProdSpecNRat> C5ProdSpecNRat { get; set; }
        public virtual DbSet<C5ArcProdSpecNutr> C5ProdSpecNutr { get; set; }
        public virtual DbSet<C5ProdStringDef> C5ProdStringDef { get; set; }
        public virtual DbSet<C5ProdStringItem> C5ProdStringItem { get; set; }
        public virtual DbSet<C5ArcProdTFDef> C5ProdTrialFormDef { get; set; }
        public virtual DbSet<C5ArcProdTFItem> C5ProdTrialFormItem { get; set; }
        public virtual DbSet<C5ProdTrialFormNutr> C5ProdTrialFormNutr { get; set; }
        public virtual DbSet<C5ProdTypeDef> C5ProdTypeDef { get; set; }

        /// <summary>
        /// System Controls
        /// </summary>
        public virtual DbSet<C5SysCtl> C5SysCtl { get; set; }

        /// <summary>
        /// Ingreidnet Factoring Nutrient Controls
        /// </summary>
        public virtual DbSet<C5FactorCtl> C5FactorCtl { get; set; }

        /// <summary>
        /// Mix Messages
        /// </summary>
        public virtual DbSet<C5MixMessage> C5MixMessage { get; set; }

        /// <summary>
        /// Archived product
        /// </summary>
        public virtual DbSet<C5ArcProdVersion> C5ArcProdVersion { get; set; }

        /// <summary>
        /// Archived Ingredient
        /// </summary>
        public virtual DbSet<C5ArcIngrVersion> C5ArcIngrVersion { get; set; }

        /// <summary>
        /// Plant Definitions
        /// </summary>
        public virtual DbSet<C5PlantDef> C5PlantDef { get; set; }
    }
}
