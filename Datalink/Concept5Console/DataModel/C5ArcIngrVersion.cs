namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Ingredient Version Definition
    /// </summary>
    public partial class C5ArcIngrVersion
    {        

        /// <summary>
        /// Ingredient Code
        /// </summary>
        [StringLength(10)]
        public string IngrCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Date Archived mm/dd/yyyy
        /// </summary>
        [StringLength(10)]
        public string ArcDate { get; set; }

        /// <summary>
        /// Time Archived hh:mm:ss       
        /// </summary>
        [StringLength(8)]
        public string ArcTime { get; set; }

        /// <summary>
        /// Archived by Concept5 User ID
        /// </summary>
        [StringLength(8)]
        public string ArcUserID { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcIngrDef C5ArcIngrDef { get; set; }
    }
}
