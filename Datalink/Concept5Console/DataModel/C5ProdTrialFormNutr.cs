namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5ProdTrialFormNutr
    {
        [Key]
        public int C5ProdTrailFormNutrID { get; set; }

        [StringLength(8)]
        public string PlantCode { get; set; }

        [StringLength(8)]
        public string ProdCode { get; set; }

        public int? NutrNumber { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? NutrAmount { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? NutrRoundAmount { get; set; }
    }
}
