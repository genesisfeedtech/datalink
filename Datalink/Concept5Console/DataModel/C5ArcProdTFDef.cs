namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archvei Trial Formula Definitions and Header Data
    /// </summary>
    public partial class C5ArcProdTFDef
    {
        //public int C5ProdTrialFormDefID { get; set; }

        [Key, Column(Order = 0)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        [Key, Column(Order = 1)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Trial Formula Status
        /// Blank - No Formula
        /// F - Feasible
        /// I - Infeasible
        /// M - Modified
        /// S - Stored
        /// </summary>
        [StringLength(1)]
        public string TFStatus { get; set; }

        /// <summary>
        /// Trial Formula Date (mm/dd/yyyy)
        /// </summary>
        [StringLength(10)]
        public string TFDate { get; set; }

        /// <summary>
        /// Trial Formula Version Number
        /// </summary>
        public int? TFVersion { get; set; }

        /// <summary>
        /// Trial Formula Created By
        /// Blank or SP - Single Product Least Cost
        /// MP - MOP
        /// GM - Global MOP
        /// MS - MultiStep
        /// UM - User Midified
        /// </summary>
        [StringLength(2)]
        public string TFCreatedBy { get; set; }

        /// <summary>
        /// Trial Formula Optimum Weight (Y or N)
        /// </summary>
        [StringLength(1)]
        public string TFOPtWt { get; set; }

        /// <summary>
        /// Optimum Weight Minimum
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? TFOptWtMin { get; set; }

        /// <summary>
        /// Optimum Weight Maximum
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? TFOptWtMax { get; set; }

        /// <summary>
        /// Optimum Weight Solution
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? TFOptWtSol { get; set; }

        /// <summary>
        /// Trial Formula Created by User ID
        /// </summary>
        [StringLength(8)]
        public string TFUserID { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdVersion C5ArcProdVersion { get; set; }

        public virtual ICollection<C5ArcProdTFItem> C5ArcProdTFItems { get; set; }
    }
}
