namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5ProdTypeDef
    {
        public int C5ProdTypeDefID { get; set; }

        [StringLength(8)]
        public string TypeNumber { get; set; }

        [StringLength(40)]
        public string TypeDesc { get; set; }
    }
}
