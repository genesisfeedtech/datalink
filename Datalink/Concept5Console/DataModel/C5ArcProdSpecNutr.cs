namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Product Specification Nutrient Requirements
    /// </summary>
    public partial class C5ArcProdSpecNutr
    {
        //public int C5ProdSpecNutrID { get; set; }

        /// <summary>
        /// Plant Code
        /// </summary>
        [Key, Column(Order = 0)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        /// <summary>
        /// Product Code
        /// </summary>
        [Key, Column(Order = 1)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [Key, Column(Order = 2)]        
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Nutrient Number
        /// </summary>
        public int? NutrNumber { get; set; }

        /// <summary>
        /// Nutrient Minimum
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? NutrMin { get; set; }

        /// <summary>
        /// Nutrient Maximum
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? NutrMax { get; set; }

        /// <summary>
        /// Nutrient Restriction Cost
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? NutrRestCost { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdSpecDef C5ArcProdSpecDef { get; set; }
    }
}
