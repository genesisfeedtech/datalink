namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Product Spcification Nutrient Ratio Requirements
    /// </summary>
    public partial class C5ArcProdSpecNRat
    {
        //public int C5ProdSpecNRatID { get; set; }

        /// <summary>
        /// Plant Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        /// <summary>
        /// Product Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Left Nutrient Number
        /// </summary>
        public int? NutrL { get; set; }

        /// <summary>
        /// Minimum Amount Required
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? MinAmount { get; set; }

        /// <summary>
        /// Maximum Amount Allowed
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? MaxAmount { get; set; }

        /// <summary>
        /// Per Amount
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? PerAmount { get; set; }

        /// <summary>
        /// Right Nutrient Number
        /// </summary>
        public int? NutrR { get; set; }

        /// <summary>
        /// Ratio Restriction Cost
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? RatRestCost { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdSpecDef C5ArcProdSpecDef { get; set; }
    }
}
