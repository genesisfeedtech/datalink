namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5IngrStringItem
    {
        public int C5IngrStringItemID { get; set; }

        [StringLength(8)]
        public string StringCode { get; set; }

        [StringLength(8)]
        public string IngrCode { get; set; }
    }
}
