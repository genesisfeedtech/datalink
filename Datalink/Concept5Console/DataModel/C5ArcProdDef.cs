namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5ArcProdDef
    {
        //public int C5ProdDefID { get; set; }

        [Key, Column(Order = 0)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        [Key, Column(Order = 1)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ArcVersion { get; set; }

        [StringLength(40)]
        public string ProdName { get; set; }

        [StringLength(16)]
        public string ShortName { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? FormBatchSize { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ManufBatchSize { get; set; }

        public int? NutrClass { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DryMatterBase { get; set; }

        [StringLength(1)]
        public string LeastCost { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? FinProdMoist { get; set; }

        [StringLength(16)]
        public string AltProdCode { get; set; }

        [StringLength(1)]
        public string UseMCS { get; set; }

        [StringLength(8)]
        public string MCSCode { get; set; }

        [StringLength(1)]
        public string IsDerivProd { get; set; }

        [StringLength(8)]
        public string BaseProdCode { get; set; }

        [StringLength(4)]
        public string BagOrBulk { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? BagSize { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? BagCost { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? WholMarkUp { get; set; }

        [StringLength(1)]
        public string WholMarkUpUnit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RetMarkUp { get; set; }

        [StringLength(1)]
        public string RetMarkUpUnit { get; set; }

        public int? MixFormat { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? SellPrice { get; set; }

        [StringLength(1)]
        public string InActive { get; set; }

        [StringLength(30)]
        public string UDField1 { get; set; }

        [StringLength(8)]
        public string UDField2 { get; set; }

        [StringLength(8)]
        public string UDField3 { get; set; }

        [StringLength(8)]
        public string UDField4 { get; set; }

        [StringLength(1)]
        public string EquivIngr { get; set; }

        [StringLength(8)]
        public string EquivIngrCode { get; set; }

        //[Column(TypeName = "numeric")]
        //public decimal? ProductionTonnage { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdVersion C5ArcProdVersion { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdInType C5ArcProdInType { get; set; }

        public virtual ICollection<C5ArcProdMixMessage> C5ArcProdMixMessages { get; set; }
    }
}
