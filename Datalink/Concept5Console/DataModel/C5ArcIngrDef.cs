namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Base Ingredient Definition
    /// </summary>
    public partial class C5ArcIngrDef
    {
        //public int C5IngrDefID { get; set; }

         /// <summary>
         /// Ingredient Code
         /// </summary>
        [StringLength(10)]
        public string IngrCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Full Name
        /// </summary>
        [StringLength(40)]
        public string IngrName { get; set; }

        /// <summary>
        /// Short Name
        /// </summary>
        [StringLength(16)]
        public string ShortName { get; set; }

        /// <summary>
        /// Moisture Content (%)
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? Moisture { get; set; }

        /// <summary>
        /// Round Amount
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? RoundAmt { get; set; }

        /// <summary>
        /// Production Minimum (Lbs or Kgs)
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? ProdMin { get; set; }

        /// <summary>
        /// Alternate Code
        /// </summary>
        [StringLength(8)]
        public string AltCode { get; set; }

        /// <summary>
        /// Legacy PI Code
        /// </summary>
        [StringLength(8)]
        public string PICode { get; set; }

        /// <summary>
        /// Mix Report Scale Number
        /// </summary>
        public int? MixScale { get; set; }

        /// <summary>
        /// Mix Report Bin Number
        /// </summary>
        public int? MixBin { get; set; }

        /// <summary>
        /// Mix Report Required Sequence Number
        /// </summary>
        public int? MixSeq { get; set; }

        /// <summary>
        /// Mix Report "other" value
        /// </summary>
        public int? MixXref { get; set; }

        /// <summary>
        /// Ingr is also an Equiv Formula (Y or N)
        /// </summary>
        [StringLength(1)]
        public string EquivFormYorN { get; set; }

        /// <summary>
        /// Equiv Formula Plant Code
        /// </summary>
        [StringLength(8)]
        public string EquivFormPlant { get; set; }

        /// <summary>
        /// Equiv Formula Product Code
        /// </summary>
        [StringLength(8)]
        public string EquivFormProd { get; set; }

        [ForeignKey("IngrCode, ArcVersion")]
        public virtual C5ArcIngrVersion C5ArcIngrVersion { get; set; }

        public virtual ICollection<C5ArcIngrNutVal> C5ArcIngrNutVal { get; set; }

        [ForeignKey("IngrCode, ArcVersion")]
        public virtual C5ArcIngrInType C5ArcIngrInType { get; set; }

        [ForeignKey("IngrCode, ArcVersion")]
        public virtual C5ArcIngrTypePct C5ArcIngrTypePct { get; set; }

        public virtual ICollection<C5ArcIngrOvlDef> C5ArcIngrOvlDefs { get; set; }
    }
}
