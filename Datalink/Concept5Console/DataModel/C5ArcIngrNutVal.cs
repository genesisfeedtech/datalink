namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5ArcIngrNutVal
    {
        //public int C5IngrNutrValID { get; set; }

        /// <summary>
        /// Ingredient Code
        /// </summary>
        [StringLength(10)]
        public string IngrCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Nutrient Number
        /// </summary>
        public int? NutrNumber { get; set; }

        /// <summary>
        /// Nutrient Amount
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? NutrAmount { get; set; }

        [ForeignKey("IngrCode, ArcVersion")]
        public virtual C5ArcIngrDef C5ArcIngrDef { get; set; }
    }
}
