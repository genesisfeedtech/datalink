namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Production Formula Defintions and Header Data
    /// </summary>
    public partial class C5ArcProdPFDef
    {
        //public int C5ProdProdnFormDefID { get; set; }

        /// <summary>
        /// Plant Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        /// <summary>
        /// Product Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Production Formula Status
        /// Blank - No Formula
        /// M - Manually Modified
        /// S - Stored From Trial
        /// </summary>
        [StringLength(1)]
        public string PFStatus { get; set; }

        /// <summary>
        /// Production Formula Date (mm/dd/yyyy)
        /// </summary>
        [StringLength(10)]
        public string PFDate { get; set; }

        /// <summary>
        /// Production Formula Version Number
        /// </summary>
        public int? PFVersion { get; set; }

        /// <summary>
        /// Production Formula Created By
        /// Blank or SP - Single Product Least Cost
        /// MP - MOP
        /// GM - Global MOP
        /// MS - Multi Step
        /// UM - User Modified
        /// </summary>
        [StringLength(2)]
        public string PFCreatedBy { get; set; }

        /// <summary>
        /// Production Formula Optimum Weight (Y or N)
        /// </summary>
        [StringLength(1)]
        public string PFOPtWt { get; set; }

        /// <summary>
        /// Optimum Weight Minimum
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? PFOptWtMin { get; set; }

        /// <summary>
        /// Optimum Weight Maximum
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? PFOptWtMax { get; set; }

        /// <summary>
        /// Optimum Weight Solution
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? PFOptWtSol { get; set; }

        /// <summary>
        /// Production Formula Created By User ID
        /// </summary>
        [StringLength(8)]
        public string PFUserID { get; set; }

        //        [StringLength(1)]
        //        public string CostType { get; set; }

        //        [Column(TypeName = "numeric")]
        //        public decimal? Cost { get; set; }

        //        [StringLength(20)]
        //        public string Unit { get; set; }


        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdVersion C5ArcProdVersion { get; set; }

        public virtual ICollection<C5ArcProdPFItem> C5ArcProdPFItems { get; set; }

        public virtual ICollection<C5ArcProdPFNutr> C5ArcProdPFNutrs { get; set; }

    }
}
