namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Ingredient Factoring Nutrient Controls
    /// </summary>
    public partial class C5FactorCtl
    {
        public int C5FactorCtlID { get; set; }

        /// <summary>
        /// Factoring Nutrient Number
        /// </summary>
        public int? IFacNutr { get; set; }

        /// <summary>
        /// Factors Nutrients in Class
        /// </summary>
        public int? IFacCls { get; set; }
    }
}
