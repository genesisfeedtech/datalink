namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5IngrCSCost
    {
        public int C5IngrCSCostID { get; set; }

        [StringLength(8)]
        public string CSCode { get; set; }

        [StringLength(8)]
        public string IngrCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? OwnCost { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MktCost { get; set; }
    }
}
