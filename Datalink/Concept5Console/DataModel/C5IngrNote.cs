namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5IngrNote
    {
        public int C5IngrNoteID { get; set; }

        [StringLength(10)]
        public string IngrCode { get; set; }

        [StringLength(10)]
        public string NoteDate { get; set; }

        [StringLength(8)]
        public string NoteTime { get; set; }

        [StringLength(8)]
        public string UserID { get; set; }

        [StringLength(400)]
        public string NoteText { get; set; }
    }
}
