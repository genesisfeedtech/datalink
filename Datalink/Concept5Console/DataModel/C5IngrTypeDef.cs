namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5IngrTypeDef
    {
        public int C5IngrTypeDefID { get; set; }

        public int? TypeNumber { get; set; }

        [StringLength(40)]
        public string TypeDesc { get; set; }
    }
}
