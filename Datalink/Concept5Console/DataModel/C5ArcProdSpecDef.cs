namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Product Least Cost Specification Headers
    /// </summary>
    public partial class C5ArcProdSpecDef
    {
        //public int C5ProdSpecDefID { get; set; }

        [Key, Column(Order = 0)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        [Key, Column(Order = 1)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        [Key, Column(Order = 2)]        
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Formulate to Optimum Weight (Y or N)
        /// </summary>
        [StringLength(1)]
        public string OptWtYorN { get; set; }

        /// <summary>
        /// Optimum Weight Minimum % of Batch
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? OptWtMin { get; set; }

        /// <summary>
        /// Optimium Weight maximum % of Batch
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? OptWtMax { get; set; }

        /// <summary>
        /// Formulate Greater than 100 (Y or N)
        /// </summary>
        [StringLength(1)]
        public string G100YorN { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdVersion C5ArcProdVersion { get; set; }

        public virtual ICollection<C5ArcProdSpecIngr> C5ArcProdSpecIngr { get; set; }

        public virtual ICollection<C5ArcProdSpecNutr> C5ArcProdSpecNutr { get; set; }

        public virtual ICollection<C5ArcProdSpecIRat> C5ArcProdSpecIRat { get; set; }

        public virtual ICollection<C5ArcProdSpecIType> C5ArcProdSpecIType { get; set; }

        public virtual ICollection<C5ArcProdSpecNRat> C5ArcProdSpecNRat { get; set; }

    }
}
