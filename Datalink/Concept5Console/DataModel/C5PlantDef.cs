namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Plant Definition
    /// </summary>
    public partial class C5PlantDef
    {
        public int C5PlantDefID { get; set; }

        [StringLength(8)]
        public string PlantCode { get; set; }

        [StringLength(40)]
        public string PlantName { get; set; }

        [StringLength(8)]
        public string AltCode { get; set; }

        [StringLength(1)]
        public string UseAltCostPlant { get; set; }

        [StringLength(8)]
        public string AltCostPlant { get; set; }

        [StringLength(1)]
        public string UseAltOvlPlant { get; set; }

        [StringLength(8)]
        public string AltOvlPlant { get; set; }

        public int? MixFormat { get; set; }

        [StringLength(2)]
        public string MixIngCodes { get; set; }

        [StringLength(2)]
        public string MixIngOrder { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MixDefBatch { get; set; }

        [StringLength(4)]
        public string MixBCFormat { get; set; }

        [StringLength(80)]
        public string MixBCFolder { get; set; }

        [StringLength(80)]
        public string MixBCFileName { get; set; }

        [StringLength(1)]
        public string MixBCShowNuts { get; set; }

        [StringLength(2)]
        public string MixBCUseCodes { get; set; }
    }
}
