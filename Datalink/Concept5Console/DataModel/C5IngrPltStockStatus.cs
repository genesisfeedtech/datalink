namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5IngrPltStockStatus
    {
        public int C5IngrPltStockStatusID { get; set; }

        [StringLength(8)]
        public string PlantCode { get; set; }

        [StringLength(8)]
        public string IngrCode { get; set; }

        [StringLength(1)]
        public string StockStatus { get; set; }
    }
}
