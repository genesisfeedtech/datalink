namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// System Controls
    /// </summary>
    public partial class C5SysCtl
    {
        public int C5SysCtlID { get; set; }

        /// <summary>
        /// Database name        
        /// </summary>
        [StringLength(40)]
        public string DBName { get; set; }

        /// <summary>
        /// US or Metric measurement (U or M)
        /// </summary>
        [StringLength(1)]
        public string USorMetric { get; set; }

        /// <summary>
        /// Ingredient Cost Units
        /// P - $/LB or Kg
        /// C - $/100Lb or 100Kg
        /// T - $/Ton or Tonne
        /// </summary>
        [StringLength(1)]
        public string ICUnits { get; set; }

        /// <summary>
        /// Ingredient Restriction Units
        /// % - Percent of Batch
        /// L - Lbs or Kgs
        /// </summary>
        [StringLength(1)]
        public string IRUnits { get; set; }

        /// <summary>
        /// Formulate on Owning or Market  (O or M)
        /// </summary>
        [StringLength(1)]
        public string LCOwnorMkt { get; set; }

        /// <summary>
        /// Adj Rounded Formula to Batch (Y or N)
        /// </summary>
        [StringLength(1)]
        public string AdjRF { get; set; }

        /// <summary>
        /// Moisture Nutrient number
        /// </summary>
        public int? nnoMoist { get; set; }

        /// <summary>
        /// Dry Matter Nutrient number
        /// </summary>
        public int? nnoDM { get; set; }

        /// <summary>
        /// Product Select Field 1 Description
        /// </summary>
        [StringLength(24)]
        public string ProdSF1Des { get; set; }

        /// <summary>
        /// Product Select Field 2 Description
        /// </summary>
        [StringLength(24)]
        public string ProdSF2Des { get; set; }

        /// <summary>
        /// Product Select Field 3 Description
        /// </summary>
        [StringLength(24)]
        public string ProdSF3Des { get; set; }

        /// <summary>
        /// Product Select Field 4 Description
        /// </summary>
        [StringLength(24)]
        public string ProdSF4Des { get; set; }

        /// <summary>
        /// Legacy Wholesale Markup
        /// % - Percent
        /// T - $/Ton
        /// </summary>
        [StringLength(1)]
        public string WMkUpFl { get; set; }

        /// <summary>
        /// Legacy Retail Markup
        /// % - Percent
        /// T - $/Ton
        /// </summary>
        [StringLength(1)]
        public string RMkUpFl { get; set; }
    }
}
