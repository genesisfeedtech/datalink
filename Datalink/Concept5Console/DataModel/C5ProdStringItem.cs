namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5ProdStringItem
    {
        public int C5ProdStringItemID { get; set; }

        [StringLength(8)]
        public string StringCode { get; set; }

        [StringLength(8)]
        public string ProdCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ProdTons { get; set; }
    }
}
