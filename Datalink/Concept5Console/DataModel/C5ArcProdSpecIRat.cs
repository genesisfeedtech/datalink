namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Archive Product Specification Ingredient Ratio Requirements
    /// </summary>
    public partial class C5ArcProdSpecIRat
    {
        //public int C5ProdSpecIRatID { get; set; }

        /// <summary>
        /// Plant Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        /// <summary>
        /// Product Code
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Left Ingredient or Type (I or T)
        /// </summary>
        [StringLength(1)]
        public string IngOrTypeL { get; set; }

        /// <summary>
        /// Left Ing or Type Code
        /// </summary>
        [StringLength(8)]
        public string CodeL { get; set; }

        /// <summary>
        /// Minimum Amount Required
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? MinAmount { get; set; }

        /// <summary>
        /// Maximum Amount Allowed
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? MaxAmount { get; set; }

        /// <summary>
        /// Per Amount
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? PerAmount { get; set; }

        /// <summary>
        /// Right Ingredient or Type (I or T)
        /// </summary>
        [StringLength(1)]
        public string IngOrTypeR { get; set; }

        /// <summary>
        /// Right Ing or Type Code
        /// </summary>
        [StringLength(8)]
        public string CodeR { get; set; }

        /// <summary>
        /// Ratio Restriction Cost
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? RatRestCost { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdSpecDef C5ArcProdSpecDef { get; set; }
    }
}
