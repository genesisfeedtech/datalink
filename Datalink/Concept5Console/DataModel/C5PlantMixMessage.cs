namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5PlantMixMessage
    {
        public int C5PlantMixMessageID { get; set; }

        [StringLength(8)]
        public string PlantCode { get; set; }

        public int? MixMsgSeq { get; set; }

        public int? MixMsgNum { get; set; }
    }
}
