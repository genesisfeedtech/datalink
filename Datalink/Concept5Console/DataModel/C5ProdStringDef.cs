namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5ProdStringDef
    {
        public int C5ProdStringDefID { get; set; }

        [StringLength(8)]
        public string StringCode { get; set; }

        [StringLength(40)]
        public string StringName { get; set; }
    }
}
