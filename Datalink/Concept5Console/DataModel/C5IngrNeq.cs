namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5IngrNeq
    {
        public int C5IngrNeqID { get; set; }

        [StringLength(8)]
        public string IngrCode { get; set; }

        public int? NutrNumber { get; set; }

        [StringLength(40)]
        public string NeqDesc { get; set; }

        public int? ReqSeq { get; set; }

        [StringLength(1)]
        public string CondFlag { get; set; }

        public int? CondNN1 { get; set; }

        [StringLength(2)]
        public string Cond1 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Cond1Amt { get; set; }

        [StringLength(1)]
        public string CondAndOr { get; set; }

        public int? CondNN2 { get; set; }

        [StringLength(2)]
        public string Cond2 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Cond2Amt { get; set; }

        [StringLength(1)]
        public string AllowNeg { get; set; }

        [StringLength(1)]
        public string SetToZero { get; set; }

        [StringLength(160)]
        public string NEQ { get; set; }
    }
}
