namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Mix Messages
    /// </summary>
    public partial class C5MixMessage
    {
        public int C5MixMessageID { get; set; }

        /// <summary>
        /// Mix Message Number
        /// </summary>
        public int? MixMsgNum { get; set; }

        /// <summary>
        /// Mix Message Text
        /// </summary>
        [StringLength(80)]
        public string MixMsgText { get; set; }
    }
}
