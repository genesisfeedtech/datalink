namespace Concept5Console.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class C5ArcProdSpecIngr
    {
        //public int C5ProdSpecIngrID { get; set; }

        /// <summary>
        /// Plant Code
        /// </summary>
        [Key, Column(Order = 0)]
        [StringLength(8)]
        public string PlantCode { get; set; }

        /// <summary>
        /// ProductCode
        /// </summary>
        [Key, Column(Order = 1)]
        [StringLength(8)]
        public string ProdCode { get; set; }

        /// <summary>
        /// Archive Version
        /// </summary>
        [Key, Column(Order = 2)]
        [StringLength(8)]
        public string ArcVersion { get; set; }

        /// <summary>
        /// Ingredient Code
        /// </summary>
        [StringLength(8)]
        public string IngrCode { get; set; }

        /// <summary>
        /// Ingredient Minimum (Frac Part of Batch)
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? IngrMin { get; set; }

        /// <summary>
        /// Ingredient Maximum (Frac Part of Batch)
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? IngrMax { get; set; }

        /// <summary>
        /// Ingredient Suppressed (Y or N)
        /// </summary>
        [StringLength(1)]
        public string IngrSup { get; set; }

        /// <summary>
        /// Maximum Change Amount (Frac Part of Batch)
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? IngrMaxChg { get; set; }

        /// <summary>
        /// Least Cost Formula Amount (Frac Part of Batch)
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? IngrAmount { get; set; }

        /// <summary>
        /// Ingredient Cost ($/Lb or Kg)
        /// </summary>        
        [Column(TypeName = "numeric")]
        public decimal? IngrCost { get; set; }

        /// <summary>
        /// Ingredient Cost Low Range ($/Lb or Kg)
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? IngrCostLowR { get; set; }

        /// <summary>
        /// Ingredient Cost High Range ($/Lb or Kg)
        /// </summary>
        [Column(TypeName = "numeric")]
        public decimal? IngrCostHighR { get; set; }

        [ForeignKey("PlantCode, ProdCode, ArcVersion")]
        public virtual C5ArcProdSpecDef C5ArcProdSpecDef { get; set; }
    }
}
