﻿using Common;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace Concept5Console
{
    public class Configuration
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public readonly String CONFIGOPTION_SQLCONNECTION = "sqlConnectionString";

        IDictionary<String, JsonModel> Config;

        public Configuration(JsonModel integrationDetailResponse)
        {
            ParseConfig(integrationDetailResponse.GetList("configOptions"));
        }

        private void ParseConfig(List<JsonModel> options)
        {
            Config = new Dictionary<String, JsonModel>();
            foreach (JsonModel entry in options)
            {
                Config[entry.GetString("name")] = entry;
            }
        }

        public String GetValue(String property, String defaultValue = null)
        {
            String value = Config.ContainsKey(property) ? (Config[property]).GetString("value") : defaultValue;
            logger.Debug("Config: {0} is {1}", property, value);
            return value;
        }

        public DateTime? GetDateTime(String property, DateTime? defaultValue = null)
        {
            DateTime? result = defaultValue;
            var stringValue = GetValue(property);
            if (stringValue != null)
            {
                try
                {
                    result = DateTime.ParseExact(stringValue, "o", CultureInfo.InvariantCulture);
                }
                catch (Exception e)
                {
                    logger.Error(e, "Failed to parse {0} as date time", stringValue);
                }
            }
            return result;
        }

        public int? GetInt(String property, int? defaultValue = null)
        {            
            var stringValue = GetValue(property);            
            int parsed;
            if (int.TryParse(stringValue, out parsed))
            {
                return parsed;
            }
            else
            {                
                logger.Error("Failed to parse {0} as int", stringValue);
                return defaultValue;
            }            
        }

        public string ToString(DateTime? value)
        {
            if (value.HasValue)
            {
                return value.Value.ToString("o", CultureInfo.InvariantCulture);
            }
            else
            {
                return null;
            }
        }

        public TimeSpan GetHeartbeatInterval()
        {
            String rawInterval = GetValue("heartbeatIntervalMinutes", "5");
            long minutes = long.Parse(rawInterval);
            return TimeSpan.FromMinutes(minutes);
        }

        public TimeSpan GetSyncInterval()
        {
            String rawInterval = GetValue("syncIntervalSeconds", "300");
            long seconds = long.Parse(rawInterval);
            return TimeSpan.FromSeconds(seconds);
        }          

        public String GetSqlConnectionString()
        {
            return GetValue(CONFIGOPTION_SQLCONNECTION);
        }
    }
}
