﻿using Common.Logging;
using NLog;
using System.Diagnostics;

namespace Concept5Console
{
    class Program
    {
        static void Main(string[] args)
        {
            new Logging().Init();

            Coordinator coordinator = new Coordinator();
            coordinator.Start();
        }
    }
}
