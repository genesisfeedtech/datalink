﻿using Common.IPC;
using NLog;
using System;
using System.Diagnostics;
using System.ServiceModel;

namespace Tray
{
    public class TrayService : ITrayService
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private object _ControlPanelLock = new object();
        private ControlPanel ControlPanel { get; set; }

        private Credentials Credentials { get; set; }
        private bool SyncRequested { get; set; }

        public void RequestSync()
        {
            SyncRequested = true;
        }

        public void UpdateCredentials(string host, string apiKey)
        {
            Credentials = new Credentials()
            {
                ServerUrl = host,
                ApiKey = apiKey
            };
        }

        public void CloseControlPanel()
        {
            logger.Debug("TrayService.CloseControlPanel");
            lock (_ControlPanelLock)
            {
                ControlPanel = null;
            }
        }

        public ControlPanel GetControlPanel()
        {         
            logger.Debug("TrayService.GetControlPanel");
            lock (_ControlPanelLock)
            {
                if (ControlPanel == null)
                {
                    ControlPanel = new ControlPanel(this);
                }
                return ControlPanel;
            }
        }

        public TrayCommand UpdateStatus(Status status)
        {
            lock (_ControlPanelLock)
            {
                if (ControlPanel != null && ControlPanel.IsClosed)
                {
                    logger.Debug("ControlPanel is closed");
                    ControlPanel = null;
                }

                if (ControlPanel != null)
                {
                    logger.Debug("TrayService.UpdateStatus");
                    try
                    {
                        ControlPanel.UpdateStatus(status);
                    }
                    catch (ObjectDisposedException e)
                    {
                        ControlPanel = null;
                        logger.Error(e, "Failed to update ControlPanel");
                    }
                }
                else
                {
                    logger.Debug("TrayService.UpdateStatus -  null ControlPanel");
                }
            }


            var command = new TrayCommand()
            {
                UpdatedCredentials = Credentials,
                ForceSync = SyncRequested
            };

            Credentials = null;
            SyncRequested = false;
            return command;
        }
    }
}
