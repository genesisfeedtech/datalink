﻿using Common.IPC;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tray
{
    public partial class ControlPanel : Form
    {
        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private TrayService TrayService { get; set; }
        private bool ConnectionEdited { get; set; } = false;

        static readonly object _StatusLock = new object();
        private Status Status;
        private DateTime LastStatusUpdate = DateTime.MinValue;
        private Thread StatusUpdater;

        public bool IsClosed { get; private set; } = false;

        public ControlPanel(TrayService trayService)
        {
            InitializeComponent();
            this.TrayService = trayService;
            StartStatusUpdater();
        }

        private void ControlPanel_Load(object sender, EventArgs e)
        {
        }

        private void saveButton_Click(object sender, EventArgs e)
        {            
            logger.Debug("Save button clicked, URL: {0}, API Key: {1}", serverUrl.Text, apiKey.Text);
            TrayService.UpdateCredentials(serverUrl.Text, apiKey.Text);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.IsClosed = true;
            try
            {
                this.StatusUpdater.Abort();
                this.StatusUpdater = null;
            }
            catch (Exception x)
            {
                logger.Debug(x, "Exception raised halting StatusUpdater");
            }
            this.TrayService.CloseControlPanel();
            this.TrayService = null;
            base.OnClosing(e);
        }

        public void UpdateStatus(Status status)
        {
            lock (_StatusLock)
            {
                this.Status = status;
                this.LastStatusUpdate = DateTime.Now;
            }
        }

        private void StartStatusUpdater()
        {
            StatusUpdater = new Thread(new ThreadStart(UpdateStatusLoop));
            StatusUpdater.Start();
        }

        private void UpdateStatusLoop()
        {
            while (!IsClosed)
            {
                if (Visible)
                {
                    DoUpdateStatus();
                }

                Thread.Sleep(100);
            }
        }

        delegate void UpdateStatusCallback();

        public void DoUpdateStatus()
        {
            if (IsClosed)
            {
                logger.Debug("controlPanel is closed");
            }
            else if (InvokeRequired)
            {
                logger.Debug("DoUpdateStatus - InvokeRequired {0}", Thread.CurrentThread.ManagedThreadId);
                var d = new UpdateStatusCallback(DoUpdateStatus);
                Invoke(d);
            }
            else
            {
                logger.Debug("DoUpdateStatus {0}", Thread.CurrentThread.ManagedThreadId);
                lock (_StatusLock)
                {
                    if (DateTime.Now.Subtract(LastStatusUpdate).TotalSeconds > 2)
                    {
                        serviceNotDetectedWarning.Visible = true;
                        Status = null;
                    }
                    else
                    {
                        serviceNotDetectedWarning.Visible = false;
                    }
                    
                    if (!ConnectionEdited)
                    {
                        serverUrl.Text = Status?.Credentials?.ServerUrl;
                        apiKey.Text = Status?.Credentials?.ApiKey;
                    }
                    PlatformConnectionSuccess.Checked = Status != null && Status.PlatformConnectionSuccessful;
                    if (Status != null)
                    {
                        LastHeartbeatText.Text = Status.LastHeartbeat.HasValue ? Status.LastHeartbeat.Value.ToLocalTime().ToString() : "--";
                        LastSyncText.Text = Status.LastSync.HasValue ? Status.LastSync.Value.ToLocalTime().ToString() : "--";

                        if (Status.Messages != null)
                        {
                            foreach (string message in Status.Messages)
                            {
                                Messages.Items.Add(message);
                            }
                        }
                    }
                }
            }            
        }

        private void RequestSyncButton_Click(object sender, EventArgs e)
        {
            TrayService.RequestSync();
        }

        private void serverUrl_TextChanged(object sender, EventArgs e)
        {
            this.ConnectionEdited = true;                 
        }

        private void apiKey_TextChanged(object sender, EventArgs e)
        {
            this.ConnectionEdited = true;
        }
    }
}
