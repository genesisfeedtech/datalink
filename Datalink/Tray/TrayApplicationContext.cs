﻿using Common.IPC;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tray
{
    public class TrayApplicationContext : ApplicationContext
    {
        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private System.Windows.Forms.NotifyIcon trayIcon;
        private System.Windows.Forms.ContextMenu contextMenu1;
        private System.Windows.Forms.MenuItem menuItemSettings;
        private System.Windows.Forms.MenuItem menuItemExit;
        private System.ComponentModel.IContainer components;

        ServiceHost host;
        TrayService TrayService;

        public TrayApplicationContext()
        {
            CreateNotifyicon();
            StartServiceHost();
        }

        private void CreateNotifyicon()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();


            // Initialize Show Settings menu item
            this.menuItemSettings = new System.Windows.Forms.MenuItem();
            this.menuItemSettings.Index = 0;
            this.menuItemSettings.Text = "&Settings";
            this.menuItemSettings.Click += new System.EventHandler(this.menuItem_Settings_Click);

            // Initialize Exit menu item
            this.menuItemExit = new System.Windows.Forms.MenuItem();
            this.menuItemExit.Index = 1;
            this.menuItemExit.Text = "E&xit";
            this.menuItemExit.Click += new System.EventHandler(this.menuItem_Exit_Click);

            // Initialize contextMenu1
            this.contextMenu1.MenuItems.AddRange(
                        new System.Windows.Forms.MenuItem[] {
                            this.menuItemSettings,
                            this.menuItemExit }
                        );

            // Create the NotifyIcon.
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);

            // The Icon property sets the icon that will appear
            // in the systray for this application.
            try
            {
                var assembly = Assembly.GetExecutingAssembly();
                var stream = assembly.GetManifestResourceStream($"Tray.gft_icon.ico");
                trayIcon.Icon = new Icon(stream);
            }
            catch (Exception e)
            {
                logger.Error(e, "Failed to load icon");
            }
            // The ContextMenu property sets the menu that will
            // appear when the systray icon is right clicked.
            trayIcon.ContextMenu = this.contextMenu1;

            // The Text property sets the text that will be displayed,
            // in a tooltip, when the mouse hovers over the systray icon.
            trayIcon.Text = "Datalink Toolbox";
            trayIcon.Visible = true;

            // Handle the DoubleClick event to activate the form.
            trayIcon.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            trayIcon.Click += new System.EventHandler(this.notifyIcon1_Click);

        }
        private void notifyIcon1_Click(object Sender, EventArgs e)
        {
            logger.Debug("clicked");
            //    MessageBox.Show("clicked");
        }

        private void notifyIcon1_DoubleClick(object Sender, EventArgs e)
        {
            logger.Debug("double clicked");
            //MessageBox.Show("Double clicked");
        }

        private void menuItem_Settings_Click(object Sender, EventArgs e)
        {
            // Close the form, which closes the application.
            logger.Debug("Settings selected");
            if (GetControlPanel().Visible)
            {
                logger.Debug("Control panel is already visible");
            }
            else
            {
                GetControlPanel().Show();
            }
        }

        private void menuItem_Exit_Click(object Sender, EventArgs e)
        {
            // Close the form, which closes the application.
            logger.Debug("Exit selected");
            trayIcon.Visible = false;
            host.Close();
            host.Abort();
            Application.Exit();
        }

        private ControlPanel GetControlPanel()
        {
            return this.TrayService.GetControlPanel();
        }

        private void StartServiceHost()
        {
            this.TrayService = new TrayService();
            host = new ServiceHost(TrayService, new Uri[] { new Uri("net.pipe://localhost") });

            //https://stackoverflow.com/questions/8902203/programmatically-set-instancecontextmode
            host.Description.Behaviors.Find<ServiceBehaviorAttribute>().InstanceContextMode = InstanceContextMode.Single;

            host.AddServiceEndpoint(typeof(ITrayService), new NetNamedPipeBinding(), "GenesisFeedTech_DatalinkTray");
            host.Open();
        }

    }
}
