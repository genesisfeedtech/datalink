﻿namespace Tray
{
    partial class ControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPanel));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.serverUrl = new System.Windows.Forms.TextBox();
            this.apiKey = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.PlatformConnectionSuccess = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.LastHeartbeatText = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LastSyncText = new System.Windows.Forms.Label();
            this.Messages = new System.Windows.Forms.ListBox();
            this.RequestSyncButton = new System.Windows.Forms.Button();
            this.serviceNotDetectedWarning = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server URL";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(67, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "API Key";
            // 
            // serverUrl
            // 
            this.serverUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverUrl.Location = new System.Drawing.Point(164, 12);
            this.serverUrl.Name = "serverUrl";
            this.serverUrl.Size = new System.Drawing.Size(565, 30);
            this.serverUrl.TabIndex = 2;
            this.serverUrl.TextChanged += new System.EventHandler(this.serverUrl_TextChanged);
            // 
            // apiKey
            // 
            this.apiKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.apiKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apiKey.Location = new System.Drawing.Point(164, 58);
            this.apiKey.Multiline = true;
            this.apiKey.Name = "apiKey";
            this.apiKey.Size = new System.Drawing.Size(565, 130);
            this.apiKey.TabIndex = 3;
            this.apiKey.TextChanged += new System.EventHandler(this.apiKey_TextChanged);
            // 
            // saveButton
            // 
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.Location = new System.Drawing.Point(622, 194);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(107, 36);
            this.saveButton.TabIndex = 4;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // PlatformConnectionSuccess
            // 
            this.PlatformConnectionSuccess.AutoSize = true;
            this.PlatformConnectionSuccess.Font = new System.Drawing.Font("Georgia", 12F);
            this.PlatformConnectionSuccess.Location = new System.Drawing.Point(12, 242);
            this.PlatformConnectionSuccess.Name = "PlatformConnectionSuccess";
            this.PlatformConnectionSuccess.Size = new System.Drawing.Size(309, 28);
            this.PlatformConnectionSuccess.TabIndex = 6;
            this.PlatformConnectionSuccess.Text = "Platform Connection Succesful";
            this.PlatformConnectionSuccess.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Georgia", 12F);
            this.label3.Location = new System.Drawing.Point(8, 273);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 24);
            this.label3.TabIndex = 7;
            this.label3.Text = "Last Heartbeat";
            // 
            // LastHeartbeatText
            // 
            this.LastHeartbeatText.AutoSize = true;
            this.LastHeartbeatText.Font = new System.Drawing.Font("Georgia", 12F);
            this.LastHeartbeatText.Location = new System.Drawing.Point(156, 273);
            this.LastHeartbeatText.Name = "LastHeartbeatText";
            this.LastHeartbeatText.Size = new System.Drawing.Size(24, 24);
            this.LastHeartbeatText.TabIndex = 8;
            this.LastHeartbeatText.Text = "--";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Georgia", 12F);
            this.label4.Location = new System.Drawing.Point(8, 297);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "Last Sync";
            // 
            // LastSyncText
            // 
            this.LastSyncText.AutoSize = true;
            this.LastSyncText.Font = new System.Drawing.Font("Georgia", 12F);
            this.LastSyncText.Location = new System.Drawing.Point(109, 297);
            this.LastSyncText.Name = "LastSyncText";
            this.LastSyncText.Size = new System.Drawing.Size(24, 24);
            this.LastSyncText.TabIndex = 10;
            this.LastSyncText.Text = "--";
            // 
            // Messages
            // 
            this.Messages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Messages.Font = new System.Drawing.Font("Georgia", 10F);
            this.Messages.FormattingEnabled = true;
            this.Messages.HorizontalScrollbar = true;
            this.Messages.ItemHeight = 20;
            this.Messages.Location = new System.Drawing.Point(12, 325);
            this.Messages.Name = "Messages";
            this.Messages.Size = new System.Drawing.Size(717, 144);
            this.Messages.TabIndex = 11;
            // 
            // RequestSyncButton
            // 
            this.RequestSyncButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RequestSyncButton.Font = new System.Drawing.Font("Georgia", 12F);
            this.RequestSyncButton.Location = new System.Drawing.Point(622, 282);
            this.RequestSyncButton.Name = "RequestSyncButton";
            this.RequestSyncButton.Size = new System.Drawing.Size(107, 37);
            this.RequestSyncButton.TabIndex = 12;
            this.RequestSyncButton.Text = "Sync";
            this.RequestSyncButton.UseVisualStyleBackColor = true;
            this.RequestSyncButton.Click += new System.EventHandler(this.RequestSyncButton_Click);
            // 
            // serviceNotDetectedWarning
            // 
            this.serviceNotDetectedWarning.AutoSize = true;
            this.serviceNotDetectedWarning.Font = new System.Drawing.Font("Georgia", 12F);
            this.serviceNotDetectedWarning.ForeColor = System.Drawing.Color.Red;
            this.serviceNotDetectedWarning.Location = new System.Drawing.Point(12, 206);
            this.serviceNotDetectedWarning.Name = "serviceNotDetectedWarning";
            this.serviceNotDetectedWarning.Size = new System.Drawing.Size(212, 24);
            this.serviceNotDetectedWarning.TabIndex = 13;
            this.serviceNotDetectedWarning.Text = "Service Not Connected";
            // 
            // ControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 473);
            this.Controls.Add(this.serviceNotDetectedWarning);
            this.Controls.Add(this.RequestSyncButton);
            this.Controls.Add(this.Messages);
            this.Controls.Add(this.LastSyncText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LastHeartbeatText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PlatformConnectionSuccess);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.apiKey);
            this.Controls.Add(this.serverUrl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ControlPanel";
            this.Text = "ControlPanel";
            this.Load += new System.EventHandler(this.ControlPanel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox serverUrl;
        private System.Windows.Forms.TextBox apiKey;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.CheckBox PlatformConnectionSuccess;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LastHeartbeatText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LastSyncText;
        private System.Windows.Forms.ListBox Messages;
        private System.Windows.Forms.Button RequestSyncButton;
        private System.Windows.Forms.Label serviceNotDetectedWarning;
    }
}