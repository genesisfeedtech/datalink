﻿using Common.Logging;
using NLog;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Tray
{

    class Program
    {
        [STAThread]
        static void Main()
        {
            new Logging().Init();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);           
            Application.Run(new TrayApplicationContext());
        }         
    }
}
