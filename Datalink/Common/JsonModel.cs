﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace Common
{
    public class JsonModel
    {
        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private JObject obj;

        public JsonModel(String json)
        {                
            obj = JsonConvert.DeserializeObject<JObject>(json);
        }

        public JsonModel(JObject obj)
        {
            this.obj = obj;
        }

        public JsonModel Get(String property)
        {
            var value = GetValue(property);
            if (value is JObject)
            {
                return new JsonModel(value as JObject);
            }
            else
            {                
                logger.Info($"{0} is {1}", property, value);
                return null;
            }
        }

        public JToken GetValue(String property)
        {
            JToken value = null;
            if (obj.ContainsKey(property))
            {
                value = obj.GetValue(property);
            }

            return value;
        }

        public String GetString(String property)
        {
            var value = GetValue(property);
            if (value is JValue)
            {
                object valueObj = ((JValue)value).Value;
                if (valueObj is DateTime)
                {
                    return ((DateTime)valueObj).ToString("o", CultureInfo.InvariantCulture);
                }
                else if (valueObj is DateTimeOffset)
                {
                    return ((DateTimeOffset)valueObj).ToString("o", CultureInfo.InvariantCulture);
                }
                return valueObj.ToString();
            }

            logger.Warn("{0} = {1}: is not a String", property, value);
            return null;
        }

        public List<JsonModel> GetList(String property)
        {
            var value = GetValue(property);

            if (value is JArray) {
                List<JsonModel> result = new List<JsonModel>();
                foreach (JObject entry in (JArray)value)
                {
                    result.Add(new JsonModel(entry));
                }
                return result;
            }

            logger.Warn("{0} = {1}: is not a list", property, value);
            return null;
        }
    }
}
