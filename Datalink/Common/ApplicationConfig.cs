﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ApplicationConfig
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static String Get(string key, string defaultValue = null)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                var value = settings[key];
                if (value == null || String.IsNullOrEmpty(value.Value))
                {
                    return defaultValue;
                } else
                {
                    return value.Value;
                }
            }
            catch (Exception e)
            {
                logger.Error(e, "Error reading app settings");
                return defaultValue;
            }
        }

        public static void Set(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (Exception e)
            {
                logger.Error(e, "Error writing app settings");
            }
        }

    }
}
