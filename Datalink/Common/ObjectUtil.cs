﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ObjectUtil
    {
        public static bool AreEqual(object a, object b)
        {
            if (a == null && b == null)
            {
                return true;
            }
            if (a != null)
            {
                return a.Equals(b);
            }
            return false;
        }
    }
}
