﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Installer
{
    public class ServiceHelper
    {
        private const int MaxAttempts = 30;
        private const int OneSecond = 1000;

        public string ServiceName { get; set; }

        public ServiceHelper(string serviceName)
        {
            this.ServiceName = serviceName;
        }

        public ServiceController GetServiceController()
        {
            ServiceController controller = ServiceController.GetServices().Where(s => s.ServiceName == ServiceName).FirstOrDefault();
            return controller;
        }

        public void StopService()
        {
            ServiceController controller = GetServiceController();
            if (controller != null)
            {                
                if ((controller.Status != ServiceControllerStatus.Stopped) && (controller.Status != ServiceControllerStatus.StopPending))
                {
                    controller.Stop();
                }
                int count = 0;
                while ((controller.Status != ServiceControllerStatus.Stopped) && count < MaxAttempts)
                {
                    count++;
                    Thread.Sleep(OneSecond);
                }
               
                if (controller.Status != ServiceControllerStatus.Stopped)
                {
                    throw new Exception($"Failed to stop service {ServiceName}");
                }
                else
                {
                    Trace.WriteLine($"Service {ServiceName} stopped");
                }
            } else
            {
                Trace.WriteLine($"Service {ServiceName} not found - nothing to stop");
            }
        }

        public void StartService()
        {
            new ServiceController(ServiceName).Start();
        }

        public void UninstallService()
        {
            StopService();
            if (GetServiceController() != null)
            {
                ServiceInstaller ServiceInstallerObj = new ServiceInstaller();
                ServiceInstallerObj.Context = new System.Configuration.Install.InstallContext();
                //ServiceInstallerObj.Context = Context;
                ServiceInstallerObj.ServiceName = ServiceName;
                ServiceInstallerObj.Uninstall(null);
                int count = 0;
                while (GetServiceController() != null && count < MaxAttempts)
                {
                    count++;
                    Thread.Sleep(OneSecond);
                }
                if (GetServiceController() == null)
                {
                    Trace.WriteLine($"Uninstall service {ServiceName} succeeded");
                }
                else
                {
                    Trace.TraceError($"Uninstall service {ServiceName} failed");
                }
            } 
            else
            {
                Trace.WriteLine($"Service {ServiceName} not found - nothing to uninstall");
            }
        }
    }
}
