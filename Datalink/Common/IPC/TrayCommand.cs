﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.IPC
{
    [DataContract]
    public class TrayCommand
    {
        [DataMember]
        public Credentials UpdatedCredentials { get; set; } = null;

        [DataMember]
        public bool ForceSync { get; set; } = false;
    }
}
