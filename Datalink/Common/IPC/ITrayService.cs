﻿using Common.IPC;
using System.Diagnostics;
using System.ServiceModel;

namespace Common.IPC
{
    [ServiceContract]
    public interface ITrayService
    {
        [OperationContract]
        TrayCommand UpdateStatus(Status status);
    }
}
