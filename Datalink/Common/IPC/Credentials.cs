﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Common.IPC
{
    [DataContract]
    public class Credentials
    {
        public Credentials() { }

        public Credentials(string serverUrl, string apiKey)
        {
            this.ServerUrl = serverUrl;
            this.ApiKey = apiKey;
        }

        [DataMember]
        public string ServerUrl { get; set; }

        [DataMember]
        public string ApiKey { get; set; }
    }
}
