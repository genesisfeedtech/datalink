﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Common.IPC
{
    [DataContract]
    public class Status
    {
        [DataMember]
        public Credentials Credentials { get; set; }

        [DataMember]
        public bool PlatformConnectionSuccessful { get; set; }

        [DataMember]
        public DateTimeOffset? LastHeartbeat { get; set; }

        [DataMember]
        public DateTimeOffset? LastSync { get; set; }

        [DataMember]
        public List<String> Messages { get; set; }
    }
}
