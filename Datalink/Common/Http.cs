﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Http
    {
        private static readonly HttpClient client = new HttpClient();

        private String Host;
        private String Token;

        public Http(String host, String token)
        {
            this.Host = host;
            this.Token = token;
        }

        public Task<HttpResponse> GetAsync(String path)
        {
            HttpRequestMessage request = BuildRequest(HttpMethod.Get, path);
            return Execute(request);
        }

        public Task<HttpResponse> PostAsync(String path, Object payload = null)
        {
            HttpRequestMessage request = BuildRequest(HttpMethod.Post, path, payload);
            return Execute(request);
        }
        
        public Task<HttpResponse> PostAsync(String path, HttpContent content)
        {
            HttpRequestMessage request = BuildRequest(HttpMethod.Post, path, content);
            return Execute(request);
        }

        public async Task<HttpResponse> Execute(HttpRequestMessage request)
        {
            var stopwatch = Stopwatch.StartNew();
            HttpResponse response = null;
            try
            {  
                HttpResponseMessage raw = await client.SendAsync(request);
                response = new HttpResponse(raw);
            }
            catch (Exception e)
            {
                response = new HttpResponse(e);
            }
            finally
            {
                stopwatch.Stop();
                response.Elapsed = stopwatch.Elapsed;
            }
            return response;
        }

        private HttpRequestMessage BuildRequest(HttpMethod method, String path, Object payload = null)
        {
            HttpContent content = null;
            if (payload != null)
            {
                String json = JsonConvert.SerializeObject(payload);
                content = new StringContent(json, Encoding.UTF8, "application/json");
            }

            return BuildRequest(method, path, content);
        }
        
        private HttpRequestMessage BuildRequest(HttpMethod method, String path, HttpContent content)
        {
            HttpRequestMessage request = new HttpRequestMessage(method, Host + path);
            request.Content = content;
            
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Token", Token);
            return request;
        }

    }
}
