﻿using Logzio.DotNet.NLog;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Common.Logging
{
    public class GFTLogzIOTarget : LogzioTarget
    {
        public string Deployment { get; set; } = "unconfigured";
        public string Hostname { get; private set; }

        public GFTLogzIOTarget()
        {
            try
            {
                this.Hostname = Dns.GetHostName();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Failed to get hostname {0}", e.Message);
            }
        }

        protected override void ExtendValues(LogEventInfo logEvent, Dictionary<string, object> values)
        {            
            values["application"] = "Datalink";
            
            if (Deployment != null)
            {
                values["deployment"] = Deployment;
            }
            if (Hostname != null)
            {
                values["hostname"] = Hostname;
            }
        }
    }
}
