﻿using Logzio.DotNet.NLog;
using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Logging
{
    public class Logging
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private bool debugMode;
        public void Init()
        {
            List<string> messages = new List<string>();
            CheckDebug();
            messages.Add($"isDebug={debugMode}");
            Trace.Listeners.Add(new ConsoleTraceListener());

            var config = new LoggingConfiguration();

            var fileName = ApplicationConfig.Get("LoggingFileName");
            messages.Add($"LoggingFileName = {fileName}");
            if (String.IsNullOrEmpty(fileName))
            {
                messages.Add("null logging file name - file logging disabled");
            }
            else
            { 
                messages.Add("Configure file logging");
                config.AddTarget("file", new FileTarget()
                {
                    FileName = fileName,
                    MaxArchiveFiles = 1,
                    ArchiveAboveSize = 100000
                });
                config.AddRule(LogLevel.Debug, LogLevel.Fatal, "file", "*");
            }

            //todo the token should be set in the build process, not stored in version control.
            String token = ApplicationConfig.Get("LoggingToken", "wGYLBmErxlpgKUXWpINwBBYKQxxcuQIC");
            messages.Add($"LoggingToken = {token}");
            if (debugMode)
            {
                messages.Add("debug mode - logz.io logging disabled");
                messages.Add("debug mode - configure console logging");
                config.AddTarget("console", new ConsoleTarget());
                config.AddRule(LogLevel.Debug, LogLevel.Fatal, "console", "*");
            }
            else
            {
                if (String.IsNullOrEmpty(token))
                {
                    messages.Add("Null logging token - logz.io logging disabled");
                } else {
                    messages.Add("Configure logz.io logging");
                    var logzioTarget = new GFTLogzIOTarget()
                    {
                        Token = token,
                    };
                    config.AddTarget("Logzio", logzioTarget);
                    config.AddRule(LogLevel.Info, LogLevel.Fatal, "Logzio", "*");                    
                }
            } 

            LogManager.Configuration = config;
            Trace.Listeners.Add(new NLogTraceListener());

            LogMessages(messages);
        }

        [Conditional("DEBUG")]
        private void CheckDebug()
        {
            this.debugMode = true;
        }

        private void LogMessages(List<string> messages)
        {
            foreach (string message in messages)
            {
                logger.Info(message);
            }
        }

    }
}
