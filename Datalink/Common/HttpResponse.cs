﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class HttpResponse
    {
        private static readonly Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static int ConnectionRefused = -1;

        HttpResponseMessage RawResponse;
        Exception Exception;
        public TimeSpan Elapsed { get; set; }

        public HttpStatusCode Status {
            get { return RawResponse != null ? RawResponse.StatusCode : (HttpStatusCode)ConnectionRefused; }
        }

        public String Reason {
            get {
                return RawResponse == null ? Exception.Message : RawResponse.ReasonPhrase;
            }
        }

        public HttpResponse(HttpResponseMessage raw)
        {
            this.RawResponse = raw;
            this.Exception = null;
        }

        public HttpResponse(Exception e)
        {
            this.Exception = e  is HttpRequestException ? e.InnerException : e;
            this.RawResponse = null;
        }

        public bool Is(int code)
        {
            return Status.Equals((HttpStatusCode)code);
        }

        public async Task<JsonModel> GetBodyAsync()
        {
            if (RawResponse != null && RawResponse.Content.Headers.ContentType.MediaType.Equals("application/json"))
            {
                String json = await RawResponse.Content.ReadAsStringAsync();
                JsonModel response = new JsonModel(json);
                return response;           
            }
            else
            {
                if (RawResponse == null)
                {
                    logger.Debug("GetBodyAsync: Null RawResponse");
                } else
                {
                    logger.Debug("GetBodyAsync: ContentType is {0}", RawResponse.Content.Headers.ContentType);
                }
                return null;
            }
        }

        public String GetBodyAsString()
        {
            if (RawResponse != null)
            {
                var stringTask = RawResponse.Content.ReadAsStringAsync();
                stringTask.Wait();
                return stringTask.Result;
            }
            else
            {
                return Exception.ToString();
            }
        }
    }
}
